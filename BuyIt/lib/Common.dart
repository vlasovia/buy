import 'dart:convert';

import 'package:BuyIt/main.dart';
import 'package:BuyIt/screens/MainScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:BuyIt/AppLocalizations.dart';

const String SHOPS_LINK = 'shopsSandbox';
const String USERS_LINK = 'usersSandbox';

final String serverToken = 'AAAAImwb2OQ:APA91bGuZam6Ce6CGg4kDtIi85vIWtTrvy8oYXroxLVyEPJQCpvMEmqVrip1Vgy-AYVtDoC_GkGaGA8sfXNmbVk7gNM6zurvnzWt5BBk9Ha0FEo-Wu8KoJoTj2Uxhpk2RyzOAvnCeWjb';

void sendNotification(String token, String body, String title, String shopId, String orderId) async {
    await http.post(
      'https://fcm.googleapis.com/fcm/send',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Authorization': 'key=$serverToken',
      },
      body: jsonEncode(
        <String, dynamic>{
          'notification': <String, dynamic>{
            'body': body,
            'title': title,
            'click_action': 'FLUTTER_NOTIFICATION_CLICK',
          },
          'priority': 'high',
          'data': <String, dynamic>{
            'click_action': 'FLUTTER_NOTIFICATION_CLICK',
            'id': '1',
            'status': 'done',
            'action': 'open_order',
            'shopId': shopId,
            'orderId': orderId,
          },
          'to': token,
        },
      ),
    );
  }

void showSnackBar(String title, String message, String buttonLabel, Function() onPressed){
  showDialog(
      context: MyApp.navigatorKey.currentContext,
      builder: (_) => AlertDialog(
        title: Text(title),
        content: Text(message),
        actions: <Widget>[
          FlatButton(
            child: Text(buttonLabel),
            onPressed: () {
              Navigator.of(MyApp.navigatorKey.currentContext).pop();
              onPressed();
            },
          ),
          FlatButton(
            child: Text(AppLocalizations.of(MyApp.navigatorKey.currentContext).translate('not_interesting')),
            onPressed: () {
              Navigator.of(MyApp.navigatorKey.currentContext).pop();
            },
          )
        ],
      ));
  // Get.snackbar('Rrrr', message, mainButton: FlatButton(onPressed: onPressed, child: Text(buttonLabel),), snackPosition: SnackPosition.BOTTOM, backgroundColor: Colors.amber);
  // try {
  //   // MainScreen.scaffoldKey.currentState.showSnackBar(
  //   Scaffold.of(MyApp.navigatorKey.currentContext).showSnackBar(
  //       SnackBar(
  //         content: Text(message),
  //         action: SnackBarAction(
  //           label: buttonLabel,
  //           onPressed: onPressed,
  //         ),
  //       )
  //   );
  // } on Exception catch (e, s) {
  //   print(s);
  // }
}

OverlayEntry showProgressIndicator(BuildContext context) {
  final overlayEntry = OverlayEntry(
    builder: (context) => Center(
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation(Theme.of(context).primaryColor),
      ),
    ),
  );
  Overlay.of(context).insert(overlayEntry);
  return overlayEntry;
}

enum ProductStatus {
  Active,
  Out,
  Disabled,
}

extension ParseToStringProductStatus on ProductStatus {
  String stringValue(BuildContext context) {
    switch (this) {
      case ProductStatus.Active:
        return 'Активный';
      case ProductStatus.Out:
        return AppLocalizations.of(context).translate('not_available');
      case ProductStatus.Disabled:
        return 'Неактивный';
    }
    return '';
  }
}

enum OrderStatus {
  New,
  Processing,
  Completed,
  Canceled,

}
// final localization = AppLocalizations.of(context);
// switch (this) {
// case ProductStatus.Active:
// return localization.translate('active');

extension ParseToStringOrderStatus on OrderStatus {
  String stringValue(BuildContext context) {
    final localization = AppLocalizations.of(context);
    switch (this) {
      case OrderStatus.New:
        return localization.translate('new');
      case OrderStatus.Processing:
        return localization.translate('processing');
      case OrderStatus.Completed:
        return localization.translate('completed');
      case OrderStatus.Canceled:
        return localization.translate('canceled');
    }
    return '';
  }
}

OrderStatus orderStatusFromString(BuildContext context, String value) {
  final localization = AppLocalizations.of(context);
  if(value == localization.translate('new')) {
    return OrderStatus.New;
  } else if (value == localization.translate('processing')) {
    return OrderStatus.Processing;
  } else if (value == localization.translate('completed')) {
      return OrderStatus.Completed;
  } else if (value == localization.translate('canceled')) {
      return OrderStatus.Canceled;
  }
  return null;
}

extension ParseToColor on OrderStatus {
  Color colorValue(BuildContext context) {
    switch (this) {
      case OrderStatus.New:
        return Theme.of(context).primaryColor;
      case OrderStatus.Processing:
        return Color.fromARGB(255, 252, 219, 73);
      case OrderStatus.Completed:
        return Colors.lightGreen;
      case OrderStatus.Canceled:
        return Colors.red;
    }
    return Colors.black12;
  }
}
