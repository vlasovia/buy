import 'package:BuyIt/Common.dart';
import 'package:BuyIt/main.dart';
import 'package:BuyIt/screens/EditProductScreen.dart';
import 'package:BuyIt/screens/ShopEditScreen.dart';
import 'package:BuyIt/screens/ShopInfoScreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:BuyIt/model/AppState.dart';
import 'package:BuyIt/model/Product.dart';
import 'package:BuyIt/model/Shop.dart';
import 'package:BuyIt/redux/actions.dart';
import 'package:BuyIt/screens/AddProductScreen.dart';
import 'package:BuyIt/screens/ProductDetailsScreen.dart';

import 'LoginScreen.dart';
import 'package:BuyIt/extensions.dart';

class ShopScreen extends StatelessWidget {
  final String shopId;

  const ShopScreen({Key key, this.shopId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, Shop>(
      onInit: (store) {
        store.dispatch(LoadShopProductsAction(context, shopId));
      },
      converter: (store) =>
          store.state.shops.firstWhere((shop) => shop.id == shopId),
      builder: (context, shop) {
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            centerTitle: true,
            title: FittedBox(
              child: Text(
                shop == null ? '' : shop.name,
              ),
            ),
            actions: <Widget>[
              StoreConnector<AppState, User>(
                  converter: (store) => store.state.user,
                  builder: (context, user) {
                    final userIsShopOwner =
                        shop != null && user.uid == shop.owner;
                    return Builder(
                      builder: (context) => IconButton(
                        icon: Icon(Icons.info),
                        color: Colors.black38,
                        onPressed: () => MyApp.navigatorKey.currentState
                            .push(MaterialPageRoute(
                                builder: (_) => userIsShopOwner
                                    ? ShopEditScreen(
                                        shop: shop,
                                        myLocale:
                                            Localizations.localeOf(context))
                                    : ShopInfoScreen(shop: shop)))
                            .then((value) {
                          if (value is Map && value['updated'] != null) {
                            StoreProvider.of<AppState>(context)
                                .dispatch(LoadShopsAction(context));
                          }
                        }),
                      ),
                    );
                  }),
              StoreConnector<AppState, User>(
                  converter: (store) => store.state.user,
                  builder: (context, user) {
                    if (shop != null && user.uid == shop.owner) {
                      return IconButton(
                        icon: Icon(Icons.add),
                        color: Colors.black38,
                        onPressed: () => MyApp.navigatorKey.currentState
                            .push(MaterialPageRoute(
                                builder: (_) =>
                                    EditProductScreen(shopId: shopId)))
                            .then((value) {
                          if (value is Map && value['updated'] != null) {
                            StoreProvider.of<AppState>(context).dispatch(
                                LoadShopProductsAction(context, shopId));
                          }
                        }),
                      );
                    }
                    return SizedBox();
                  }),
            ],
          ).withBottomAdmobBanner(context),
          body: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: ListView(
                children: _getProductsWidgets(context, shop.products),
              ),
            ),
          ),
        );
      },
    );
  }

  List<Widget> _getProductsWidgets(
      BuildContext context, List<Product> products) {
    if (products == null) return [];
    final borderRadius = BorderRadius.all(Radius.circular(16.0));
    return products
        .map(
          (product) => GestureDetector(
            onTap: () => MyApp.navigatorKey.currentState.push(MaterialPageRoute(
                builder: (_) => ProductDetailsScreen(
                      shopId: shopId,
                      productId: product.id,
                    ))),
            child: Container(
              height: 200.0,
              child: Card(
                margin: const EdgeInsets.all(16.0),
                elevation: 5,
                child: Stack(
                  children: [
                    ClipRRect(
                      child: Image.network(
                        product.imageUrl,
                        width: double.infinity,
                        fit: BoxFit.cover,
                      ),
                      borderRadius: borderRadius,
                    ),
                    if (product.status == ProductStatus.Out ||
                        product.status == ProductStatus.Disabled)
                      Container(
                        width: double.infinity,
                        height: double.infinity,
                        padding: const EdgeInsets.all(8.0),
                        decoration: BoxDecoration(
                          color: Colors.white.withAlpha(180),
                          borderRadius: borderRadius,
                        ),
                      ),
                    Container(
                      width: double.infinity,
                      height: double.infinity,
                      child: Center(
                        child: Container(
                          padding: const EdgeInsets.all(8.0),
                          decoration: BoxDecoration(
                            color: Colors.white.withAlpha(180),
                            borderRadius: borderRadius,
                          ),
                          child: Text(
                            product.title,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                shape: RoundedRectangleBorder(
                  side: BorderSide(
                    color: Theme.of(context).primaryColor,
                  ),
                  borderRadius: borderRadius,
                ),
              ),
            ),
          ),
        )
        .toList();
  }
}
