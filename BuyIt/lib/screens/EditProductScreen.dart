import 'dart:io';
import 'dart:ui';

import 'package:BuyIt/extensions.dart';
import 'package:BuyIt/model/Product.dart';
import 'package:BuyIt/redux/actions.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:image_picker/image_picker.dart';
import 'package:BuyIt/model/AppState.dart';
import 'package:BuyIt/screens/SelectProductTypeScreen.dart';
import 'package:uuid/uuid.dart';
import 'package:path/path.dart' as p;
import 'package:BuyIt/AppLocalizations.dart';

import '../Common.dart';

class EditProductScreen extends StatefulWidget {
  final String shopId;
  final Product product;

  const EditProductScreen({Key key, this.shopId, this.product})
      : super(key: key);

  @override
  _EditProductScreenState createState() => _EditProductScreenState();
}

class _EditProductScreenState extends State<EditProductScreen> {
  final _formKey = GlobalKey<FormState>();

  String productTypeId;
  String productTypeName;
  final ImagePicker _picker = ImagePicker();
  PickedFile _imageFile;
  dynamic _pickImageError;
  String _retrieveDataError;

  final picker = ImagePicker();

  String _measureDropdownValue = 'кг';
  String productStatusDropdownValue;

  Product _product;

  String _title;
  String _description;
  double _price;
  double _measurementStep;

  bool _imageNotSelected = false;

  @override
  void initState() {
    _product =
        widget.product ?? Product(measurement: 'кг', measurementStep: 0.25);
    _measureDropdownValue = _product.measurement;
    super.initState();
  }

  Future<String> _uploadFile() async {
    final String uuid = Uuid().v1();
    final fileName = '$uuid${p.extension(_imageFile.path)}';
    final StorageReference ref = FirebaseStorage()
        .ref()
        .child('sandbox')
        .child('storesProductsImages/${widget.shopId}/$fileName');
    final StorageUploadTask uploadTask = ref.putFile(
      File(_imageFile.path),
    );
    await uploadTask.onComplete;
    return fileName;
  }

  Text _getRetrieveErrorWidget() {
    if (_retrieveDataError != null) {
      final Text result = Text(_retrieveDataError);
      _retrieveDataError = null;
      return result;
    }
    return null;
  }

  Widget _previewImage() {
    if (_imageNotSelected) {
      return Text(
        AppLocalizations.of(context).translate('picture_not_being_selected'),
        textAlign: TextAlign.center,
        style: TextStyle(color: Colors.red),
      );
    }
    final Text retrieveError = _getRetrieveErrorWidget();
    if (retrieveError != null) {
      return retrieveError;
    }
    if (_imageFile != null) {
      if (kIsWeb) {
        // Why network?
        // See https://pub.dev/packages/image_picker#getting-ready-for-the-web-platform
        return Image.network(_imageFile.path);
      } else {
        return Image.file(File(_imageFile.path));
      }
    } else if (_pickImageError != null) {
      return Text(
        'Pick image error: $_pickImageError',
        textAlign: TextAlign.center,
      );
    } else if (_product.imageUrl != null) {
      return Image.network(_product.imageUrl);
    } else {
      return Text(
        AppLocalizations.of(context).translate('picture_not_being_selected'),
        textAlign: TextAlign.center,
      );
    }
  }

  Future<void> retrieveLostData() async {
    final LostData response = await _picker.getLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
      setState(() {
        _imageFile = response.file;
      });
    } else {
      _retrieveDataError = response.exception.code;
    }
  }

  Future<void> _setProduct(OverlayEntry overlayEntry, {String fileName}) async {
    // FirebaseFirestore.instance
    //     .collection(SHOPS_LINK)
    //     .doc(widget.shop.id)
    //     .collection('products')
    //     .doc()
    //     .set(
    //   {
    //     'title': titleTextEditingController.text,
    //     'description': descriptionTextEditingController.text,
    //     'price': double.parse(priceTextEditingController.text),
    //     'measurement': dropdownValue,
    //     'image': fileName,
    //   },
    // );

    productStatusDropdownValue ??=
        _product.status?.stringValue(context) ?? ProductStatus.Active.stringValue(context);
    var productJson = {
      'title': _title,
      'description': _description,
      'price': _price,
      'measurement': _measureDropdownValue,
      'measurementStep': _measurementStep,
      'status': ProductStatus.values.indexWhere((element) =>
          element.stringValue(context) ==
          (productStatusDropdownValue ??
              ProductStatus.Active.stringValue(context))),
    };
    if (fileName != null) {
      productJson['image'] = fileName;
    }
    await FirebaseFirestore.instance
        .collection(SHOPS_LINK)
        .doc(widget.shopId)
        .collection('products')
        .doc(_product?.id)
        .set(productJson, SetOptions(merge: true));
    overlayEntry.remove();
    Navigator.of(context).pop({'updated': true});
  }

  _onPressedSetButton() {
    if (_formKey.currentState.validate()) {
      if (_imageFile == null && _product.id == null) {
        setState(() {
          _imageNotSelected = true;
        });
        return;
      }
      final overlayEntry = showProgressIndicator(context);
      _formKey.currentState.save();
      _imageFile == null
          ? _setProduct(overlayEntry)
          : _uploadFile().then(
              (fileName) => _setProduct(overlayEntry, fileName: fileName),
            );
    }
  }

  @override
  Widget build(BuildContext context) {
    productStatusDropdownValue ??=
        _product.status?.stringValue(context) ?? ProductStatus.Active.stringValue(context);
    String priceString;
    if (_product?.price != null) {
      priceString = _product.price.toStringAsFixed(2);
    }
    return StoreConnector<AppState, User>(
      converter: (store) => store.state.user,
      builder: (context, user) {
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            centerTitle: true,
            title: Text(
              '${AppLocalizations.of(context).translate('edit')} ${_product.title}',
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.check),
                color: Theme.of(context).primaryColor,
                onPressed: _onPressedSetButton,
              ),
            ],
          ).withBottomAdmobBanner(context),
          body: SafeArea(
            child: GestureDetector(
              onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: SingleChildScrollView(
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        Center(
                          child: _previewImage(),
                        ),
                        TextFormField(
                          maxLines: 1,
                          initialValue: _product.title ?? '',
                          decoration: InputDecoration(
                              hintText: AppLocalizations.of(context)
                                  .translate('name')),
                          onSaved: (value) => _title = value,
                          validator: (value) {
                            if (value.isEmpty) {
                              return AppLocalizations.of(context)
                                  .translate('enter_name');
                            }
                            return null;
                          },
                        ),
                        TextFormField(
                          maxLines: 4,
                          initialValue: _product.description ?? '',
                          decoration: InputDecoration(
                              hintText: AppLocalizations.of(context)
                                  .translate('enter_description')),
                          onSaved: (value) => _description = value,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                padding: const EdgeInsets.all(8.0),
                                width: 120.0,
                                child: TextFormField(
                                  initialValue: priceString ?? '',
                                  decoration: InputDecoration(
                                    hintText: AppLocalizations.of(context)
                                        .translate('price'),
                                    suffix: Text('грн'),
                                  ),
                                  keyboardType: TextInputType.numberWithOptions(
                                      decimal: true),
                                  onSaved: (value) =>
                                      _price = double.parse(value),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return AppLocalizations.of(context)
                                          .translate('enter_price');
                                    }
                                    return null;
                                  },
                                ),
                              ),
                              DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  value: _measureDropdownValue,
                                  onChanged: (String newValue) {
                                    setState(() {
                                      _measureDropdownValue = newValue;
                                    });
                                  },
                                  isDense: true,
                                  underline: null,
                                  items: <String>['кг', 'штука']
                                      .map<DropdownMenuItem<String>>(
                                          (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ],
                          ),
                        ),
                        if (_measureDropdownValue == 'кг')
                          Container(
                            padding: const EdgeInsets.all(8.0),
                            width: 80.0,
                            child: TextFormField(
                              initialValue:
                                  _product.measurementStep.toString() ?? '',
                              decoration: InputDecoration(
                                hintText: AppLocalizations.of(context)
                                    .translate('weight_step'),
                                suffix: Text('кг'),
                              ),
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true),
                              onSaved: (value) =>
                                  _measurementStep = double.parse(value),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return AppLocalizations.of(context)
                                      .translate('enter_step');
                                }
                                return null;
                              },
                            ),
                          ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                width: 80.0,
                                child: Text('Статус'),
                              ),
                              DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  value: productStatusDropdownValue,
                                  onChanged: (String newValue) {
                                    setState(() {
                                      productStatusDropdownValue = newValue;
                                    });
                                  },
                                  isDense: true,
                                  underline: null,
                                  items: ProductStatus.values
                                      .map((ProductStatus value) {
                                    return DropdownMenuItem<String>(
                                      value: value.stringValue(context),
                                      child: Text(value.stringValue(context)),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          floatingActionButton: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FloatingActionButton(
                onPressed: () {
                  _onImageButtonPressed(ImageSource.gallery, context: context);
                },
                heroTag: 'image0',
                tooltip: AppLocalizations.of(context)
                    .translate('choose_from_gallery'),
                child: const Icon(
                  Icons.photo_library,
                  color: Colors.white,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: FloatingActionButton(
                  onPressed: () {
                    _onImageButtonPressed(ImageSource.camera, context: context);
                  },
                  heroTag: 'image1',
                  tooltip: AppLocalizations.of(context).translate('take_photo'),
                  child: const Icon(
                    Icons.camera_alt,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void _onImageButtonPressed(ImageSource source, {BuildContext context}) async {
    try {
      final pickedFile = await _picker.getImage(
        source: source,
        maxWidth: 1024.0,
        maxHeight: 1024.0,
        imageQuality: 80,
      );
      setState(() {
        _imageNotSelected = false;
        _imageFile = pickedFile;
      });
    } catch (e) {
      setState(() {
        _imageNotSelected = true;
        _pickImageError = e;
      });
    }
  }
}
