import 'dart:convert';

import 'package:BuyIt/AppLocalizations.dart';
import 'package:BuyIt/Common.dart';
import 'package:BuyIt/main.dart';
import 'package:BuyIt/extensions.dart';
import 'package:BuyIt/model/UserData.dart';
import 'package:BuyIt/screens/PastOrderScreen.dart';
import 'package:BuyIt/screens/PhoneLoginScreen.dart';
import 'package:BuyIt/services/AdManager.dart';

// import 'package:firebase_admob/firebase_admob.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:BuyIt/model/AppState.dart';
import 'package:BuyIt/model/Shop.dart';
import 'package:BuyIt/redux/actions.dart';
import 'package:BuyIt/screens/ShopScreen.dart';
import 'package:BuyIt/services/NetworkManager.dart';
import 'package:BuyIt/services/PushNotificationsManager.dart';
import 'package:BuyIt/widgets/CartButton.dart';
import 'package:BuyIt/AppLocalizations.dart';

import 'CartScreen.dart';
import 'LoginScreen.dart';
import 'OrdersScreen.dart';

class MainScreen extends StatefulWidget {
  static List<String> deliveryTypes;

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  // TODO: Add _bannerAd
  // BannerAd _bannerAd;

  void _loadBannerAd() {
    // _bannerAd
    //   ..load()
    //   ..show(
    //       anchorType: AnchorType.top,
    //     anchorOffset: 80.0,
    //     // Positions the banner ad 10 pixels from the center of the screen to the right
    //     // horizontalCenterOffset: 10.0,
    //     // Banner Position
    //     // anchorType: AnchorType.bottom,
    //   );
  }

  @override
  void initState() {
    // FirebaseAdMob.instance.initialize(appId: AdManager.appId).then((value) {
    //   // TODO: Initialize _bannerAd
    //   _bannerAd = BannerAd(
    //     adUnitId: AdManager.bannerAdUnitId,
    //     targetingInfo: MobileAdTargetingInfo(
    //       keywords: <String>['flutterio', 'beautiful apps'],
    //       contentUrl: 'https://flutter.io',
    //       childDirected: false,
    //       testDevices: <String>["A4E047867CCE48DB341BFB9D5A2F6228"], // Android emulators are considered test devices
    //     ),
    //     // size: AdSize.banner,
    //     size: AdSize.smartBanner,
    //     // targetingInfo: targetingInfo,
    //     listener: (MobileAdEvent event) {
    //       print("BannerAd event is $event");
    //     },
    //   );

    //   // TODO: Load a Banner Ad
    //   _loadBannerAd();
    // });
    super.initState();
  }

  @override
  void dispose() {
    // TODO: Dispose BannerAd object
    // _bannerAd?.dispose();
    super.dispose();
  }

  _showLogoutAlert(BuildContext buildContext) {
    return showDialog(
            context: buildContext,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text(AppLocalizations.of(context).translate('exit')),
                content: Text(AppLocalizations.of(context)
                    .translate('logout_are_you_sure')),
                actions: <Widget>[
                  FlatButton(
                    child: Text(AppLocalizations.of(context).translate('yes')),
                    onPressed: () {
                      StoreProvider.of<AppState>(context, listen: false)
                          .dispatch(UserLogOutAction());
                      Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(
                          builder: (_) => PhoneLoginScreen(),
                        ),
                        ModalRoute.withName('/'),
                      );
                    },
                  ),
                  FlatButton(
                    child:
                        Text(AppLocalizations.of(context).translate('cancel')),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              );
            }) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, User>(
//        onInit: (store) {
//          Future.delayed(Duration.zero, () {
//            store.dispatch(CheckUserAction());
//          });
//        },
      onInit: (store) {
//        final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
//        _firebaseMessaging.requestNotificationPermissions();
        MainScreen.deliveryTypes = [
          AppLocalizations.of(context).translate('delivery_to_address'),
          AppLocalizations.of(context).translate('take_by_myself')
        ];
        store.dispatch(CheckUserAction(context));
      },
      converter: (store) => store.state.user,
      builder: (context, user) {
        bool anonymousUser = user != null && user.isAnonymous;
        String userContact =
            user == null ? '' : user.email ?? user.phoneNumber ?? '';
        return Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            elevation: 0,
            backgroundColor: Colors.transparent,
            centerTitle: true,
            title: Text(
              AppLocalizations.of(context).translate('shops'),
            ),
            leading: Builder(builder: (BuildContext context) {
              return IconButton(
                icon: Icon(anonymousUser ? Icons.person : Icons.menu),
                color: Colors.black38,
                onPressed: () {
                  anonymousUser
                      ? Navigator.of(context).push(
                          MaterialPageRoute(builder: (_) => PhoneLoginScreen()))
                      : Scaffold.of(context).openDrawer();
                },
              );
            }),
            actions: <Widget>[
              // IconButton(icon: Icon(Icons.ac_unit_rounded), color: Colors.amber, onPressed: () {
              //   Future.delayed(Duration(seconds: 3), () {
              //     showSnackBar('Заголовок', 'Test', 'This is button', () {});
              //   });
              // },),
              CartButton(),
            ],
          ).withBottomAdmobBanner(context),
          drawer: Drawer(
            child: ListView(
              // Important: Remove any padding from the ListView.
              padding: EdgeInsets.zero,
              children: <Widget>[
                StoreConnector<AppState, UserData>(
                    converter: (store) => store.state.userData,
                    builder: (context, userData) {
                      return UserAccountsDrawerHeader(
                        accountName: Text(userData.firstName == null
                            ? ''
                            : userData.firstName + ' ' + userData.lastName),
                        accountEmail: Text(
                          userContact,
                          style: TextStyle(color: Colors.white),
                        ),
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                        ),
//                  currentAccountPicture: Icon(
//                    Icons.person,
//                    size: 60.0,
//                    color: Colors.white,
//                  ) //CircleAvatar(backgroundImage: Icons.person,),
                      );
                    }),
//            ListTile(
//              title: Text('Home'),
//              onTap: () {
//                setState(() {
//                  _title = 'Home';
//                });
//                _selectedIndex = 0;
//                _selectMenuItem(MenuItem.values[0]);
//              },
//            ),
//            ListTile(
//              title: Text('Resources'),
//              onTap: () {
//                setState(() {
//                  _title = 'Resources';
//                });
//                _selectedIndex = 2;
//                _selectMenuItem(MenuItem.values[2]);
//              },
//            ),
                ListTile(
                  title: Text(
                    AppLocalizations.of(context).translate('orders'),
                    style: TextStyle(color: Colors.green),
                  ),
                  onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => OrdersScreen())),
                ),
                ListTile(
                  title: Text(
                    AppLocalizations.of(context).translate('exit'),
                    style: TextStyle(color: Colors.red),
                  ),
                  onTap: () => _showLogoutAlert(
                    context,
                  ),
                ),
              ],
            ),
          ),
          body: SafeArea(
            child: StoreConnector<AppState, List<Shop>>(
              converter: (store) => store.state.shops,
              builder: (context, shops) {
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: ListView(
                    children: _getShopsWidgets(context, shops),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }

  List<Widget> _getShopsWidgets(BuildContext context, List<Shop> shops) {
    if (shops == null) return [];
    final borderRadius = BorderRadius.all(Radius.circular(16.0));
    return shops
        .map(
          (shop) => GestureDetector(
            onTap: () => MyApp.navigatorKey.currentState.push(MaterialPageRoute(
                builder: (_) => ShopScreen(
                      shopId: shop.id,
                    ))),
            child: Container(
              height: 200.0,
              child: Card(
                margin: const EdgeInsets.all(16.0),
//              margin: const EdgeInsets.symmetric(vertical: 16.0),
                elevation: 5,
//            color: backgroundColor,
                child: Stack(
                  children: [
                    ClipRRect(
                      child: Image.network(
                        shop.titleImageUrl,
                        width: double.infinity,
                        fit: BoxFit.cover,
                      ),
                      borderRadius: borderRadius,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        width: double.infinity,
                        height: double.infinity,
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            width: double.infinity,
                            height: 48.0,
                            padding: const EdgeInsets.all(8.0),
                            decoration: BoxDecoration(
                              color: Colors.white.withAlpha(210),
                              borderRadius: borderRadius,
                            ),
                            child: FittedBox(
                              child: Text(
                                shop.name,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontSize: 40.0,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                shape: RoundedRectangleBorder(
                  side: BorderSide(
                    color: Theme.of(context).primaryColor,
                  ),
                  borderRadius: borderRadius,
                ),
              ),
            ),
          ),
        )
        .toList();
  }
}
