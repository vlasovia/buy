import 'package:BuyIt/Common.dart';
import 'package:BuyIt/model/AppState.dart';
import 'package:BuyIt/model/CartProduct.dart';
import 'package:BuyIt/model/Order.dart';
import 'package:BuyIt/model/UserData.dart';
import 'package:BuyIt/redux/actions.dart';
import 'package:BuyIt/screens/PastOrderEditScreen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:BuyIt/extensions.dart';

import 'package:BuyIt/AppLocalizations.dart';

import 'ProductDetailsScreen.dart';

class PastOrderScreen extends StatefulWidget {
  final String shopId;
  final String orderId;

  const PastOrderScreen({Key key, this.shopId, this.orderId}) : super(key: key);

  @override
  _PastOrderScreenState createState() => _PastOrderScreenState();
}

class _PastOrderScreenState extends State<PastOrderScreen> {
  String _orderStatusDropdownValue;

  @override
  void initState() {
    // _orderStatusDropdownValue = widget.order.orderStatus.stringValue();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final userIsOwner =
        StoreProvider.of<AppState>(context).state.userData.myShopId ==
            widget.shopId;
    return StoreConnector<AppState, Order>(
      converter: (store) =>
          store.state.orders.firstWhere((order) => order.id == widget.orderId),
      builder: (context, order) {
        _orderStatusDropdownValue = order.orderStatus.stringValue(context);
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            centerTitle: true,
            title: Text(
              '${AppLocalizations.of(context).translate('order')} ${order.orderNumberStringValue()}',
              style: TextStyle(
                color: order.orderStatus.colorValue(context),
                fontSize: 18.0,
              ),
            ),
            // actions: <Widget>[
            //   IconButton(
            //     icon: Icon(Icons.edit_outlined),
            //     color: Theme.of(context).primaryColor,
            //     onPressed: () => Navigator.of(context).push(
            //       MaterialPageRoute(builder: (_) => PastOrderEditScreen()),
            //     ),
            //   ),
            // ],
          ).withBottomAdmobBanner(context),
          body: GestureDetector(
            onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            order.orderStatus.stringValue(context),
                            style: TextStyle(
                              color: order.orderStatus.colorValue(context),
                              fontSize: 18.0,
                            ),
                          ),
                          SizedBox(
                            width: 8.0,
                          ),
                          Text(
                            order.dateCreatedStringValue(),
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 14.0,
                            ),
                          ),
                          Expanded(child: SizedBox()),
                          Text(
                            order.orderPriceStringValue(),
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 22.0,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Container(
                        decoration: ShapeDecoration(
                          shape: RoundedRectangleBorder(
                            side: BorderSide(
                              width: 4.0,
                              color: order.orderStatus.colorValue(context),
                            ),
                            borderRadius:
                                BorderRadius.all(Radius.circular(16.0)),
                          ),
                        ),
                        child: ListView.builder(
                          itemCount: order.cartProducts.length,
                          itemBuilder: (context, index) {
                            return _productWidget(order.cartProducts[index]);
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          if (userIsOwner)
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 8.0),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  value: _orderStatusDropdownValue ??
                                      OrderStatus.New.stringValue(context),
                                  onChanged: (newValue) =>
                                      _updateOrderStatus(newValue, order),
                                  // onChanged: (String newValue) {
                                  //   setState(() {
                                  //     _orderStatusDropdownValue = newValue;
                                  //   });
                                  // },
                                  isDense: true,
                                  underline: null,
                                  items: OrderStatus.values
                                      .map((OrderStatus value) {
                                    return DropdownMenuItem<String>(
                                      value: value.stringValue(context),
                                      child: Text(
                                        value.stringValue(context),
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: value.colorValue(context),
                                        ),
                                      ),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Text(
                              order.deliveryType == 0
                                  ? '${AppLocalizations.of(context).translate('delivery_address')}:'
                                  : AppLocalizations.of(context)
                                      .translate('take_by_myself'),
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          if (order.deliveryType == 0) Text(order.address),
                          SizedBox(height: 8.0),
                          if (order.comment != null)
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 8.0),
                                  child: Text(
                                    '${AppLocalizations.of(context).translate('comment')}:',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Text(order.comment),
                              ],
                            ),
                          if (userIsOwner)
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 8.0),
                              child: InkWell(
                                  onTap: () => launch("tel://${order.phone}"),
                                  child: Text(
                                    "${AppLocalizations.of(context).translate('call')}: ${order.phone}",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Theme.of(context).primaryColor,
                                    ),
                                  )),
                            ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  _updateOrderStatus(String newValue, Order order) {
    final overlayEntry = showProgressIndicator(context);
    var orderJson = order.toJson();
    orderJson['orderStatus'] = orderStatusFromString(context, newValue).index;
    orderJson['dateCreated'] = Timestamp.fromDate(order.dateCreated);
    var updatedOrder = Order.fromJson(orderJson);
    updatedOrder.cartProducts = order.cartProducts;
    StoreProvider.of<AppState>(context).dispatch(UpdatePastOrderAction(
        context: context,
        order: updatedOrder,
        response: (data, error) {
          overlayEntry.remove();
          if (error != null) {
            Scaffold.of(context).showSnackBar(SnackBar(
              content:
                  Text(AppLocalizations.of(context).translate('common_error')),
              duration: Duration(seconds: 3),
            ));
          }
        }));
  }

  _productWidget(CartProduct cartProduct) {
    return GestureDetector(
      onTap: () => Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => ProductDetailsScreen(
                shopId: cartProduct.shopId,
                productId: cartProduct.productId,
              ))),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Text(
                cartProduct.title,
                overflow: TextOverflow.fade,
                maxLines: 5,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16.0,
                ),
              ),
            ),
            SizedBox(width: 14.0),
            Text(
              '₴${(cartProduct.quantity * cartProduct.price).toStringAsFixed(2)}/${cartProduct.quantity}',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
                fontSize: 16.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
