import 'dart:io';
import 'dart:ui';

import 'package:BuyIt/model/AppState.dart';
import 'package:BuyIt/model/Shop.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:image_picker/image_picker.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:uuid/uuid.dart';
import 'package:path/path.dart' as p;
import 'package:BuyIt/extensions.dart';
import 'package:BuyIt/AppLocalizations.dart';

import '../Common.dart';

class ShopInfoScreen extends StatefulWidget {
  final Shop shop;

  const ShopInfoScreen({Key key, this.shop}) : super(key: key);

  @override
  _ShopInfoScreenState createState() => _ShopInfoScreenState();
}

class _ShopInfoScreenState extends State<ShopInfoScreen> {
  @override
  Widget build(BuildContext context) {
    final shop = widget.shop;
    return StoreConnector<AppState, User>(
      converter: (store) => store.state.user,
      builder: (context, user) {
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            centerTitle: true,
            title: Text(
              shop.name,
            ),
          ).withBottomAdmobBanner(context),
          body: SafeArea(
            child: GestureDetector(
              onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: ListView(
                  children: [
                    Center(
                      child: Image.network(shop.titleImageUrl),
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Text(shop.description),
                    SizedBox(
                      height: 8.0,
                    ),
                    InkWell(
                        onTap: () => launch("tel://${shop.phone}"),
                        child: Text(
                            "${AppLocalizations.of(context).translate('call')}: ${shop.phone}")),
                    SizedBox(
                      height: 8.0,
                    ),
                    Text(shop.address),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
