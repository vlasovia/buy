import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:BuyIt/screens/PhoneVerificationScreen.dart';
import 'package:BuyIt/extensions.dart';
import 'package:BuyIt/AppLocalizations.dart';

import '../Common.dart';

class PhoneLoginScreen extends StatefulWidget {
  final bool moveToMainScreen;
  final String phoneNumber;

  const PhoneLoginScreen({Key key, this.moveToMainScreen = true, this.phoneNumber}) : super(key: key);

  @override
  _PhoneLoginScreenState createState() => _PhoneLoginScreenState();
}

class _PhoneLoginScreenState extends State<PhoneLoginScreen> {
  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    if (widget.phoneNumber != null) {
      _controller.text = widget.phoneNumber;
    }
    super.initState();
  }

  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ).withBottomAdmobBanner(context),
      body: SafeArea(
        child: Center(
          child: Container(
            width: 200.0,
            child: Column(
              children: [
                TextFormField(autofocus: true,
                  controller: _controller,
                  maxLength: 9,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    prefixText: "+380",
//                    icon: Icon(Icons.person),
//                    hintText: 'What do people call you?',
                    labelText: AppLocalizations.of(context).translate('mobile_phone_number'),
                  ),
                  onSaved: (String value) {
                    // This optional block of code can be used to run
                    // code when the user saves the form.
                  },
                  validator: (String value) {
                    return value.contains('@')
                        ? 'Do not use the @ char.'
                        : null;
                  },
                ),
                SizedBox(
                  height: 16.0,
                ),
                Container(
                  width: double.infinity,
                  height: 60.0,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.all(
                      Radius.circular(8.0),
                    ),
                  ),
                  child: FlatButton(
                      onPressed: () => _phoneSignIn(),
                      child: Text(
                        'Готово',
                        style: TextStyle(color: Colors.white, fontSize: 18.0),
                      )),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _phoneSignIn() async {
    final overlayEntry = showProgressIndicator(context);
    String phoneNumber = "+380${_controller.text}";
    await FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: phoneNumber,
      verificationCompleted: (PhoneAuthCredential credential) {
        overlayEntry.remove();
      },
      verificationFailed: (FirebaseAuthException e) {
        overlayEntry.remove();
      },
      codeSent: (String verificationId, int resendToken) {
        overlayEntry.remove();
        Navigator.of(context).push(MaterialPageRoute(
            builder: (_) => PhoneVerificationScreen(
                  phoneNumber: phoneNumber, verificationId: verificationId, resendToken: resendToken, moveToMainScreen: widget.moveToMainScreen,
                )));
      },
      codeAutoRetrievalTimeout: (String verificationId) {
        overlayEntry.remove();
      },
    );
//    overlayEntry.remove();
  }
}
