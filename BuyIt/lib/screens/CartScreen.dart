import 'package:BuyIt/screens/ProductDetailsScreen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:BuyIt/model/AppState.dart';
import 'package:BuyIt/model/CartProduct.dart';
import 'package:BuyIt/model/Product.dart';
import 'package:BuyIt/redux/actions.dart';
import 'package:BuyIt/widgets/Counter.dart';
import 'package:BuyIt/extensions.dart';
import 'package:BuyIt/AppLocalizations.dart';

import '../Common.dart';
import 'OrderScreen.dart';

class CartScreen extends StatefulWidget {
  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  List<Product> products;

  double price = 0.0;

  _countPrice(List<CartProduct> cartProducts) {
    price = 0.0;
    if (products != null) {
      for (var index = 0; index < products.length; ++index) {
        final product = products[index];
        final cartProduct = cartProducts[index];
        price += product.price * cartProduct.quantity;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          AppLocalizations.of(context).translate('cart'),
        ),
      ).withBottomAdmobBanner(context),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: StoreConnector<AppState, List<CartProduct>>(
              converter: (store) => store.state.cartProducts,
              builder: (context, cartProducts) {
                _countPrice(cartProducts);
                return _getContentWidget(cartProducts);
              }),
        ),
      ),
    );
  }

  Widget _getContent(Widget cartList) {
    return Column(
      children: [
        Expanded(child: cartList),
        Stack(
          children: [
            Container(
              width: double.infinity,
              height: 60.0,
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.all(
                  Radius.circular(8.0),
                ),
              ),
              child: FlatButton(
                onPressed: () {
                  if ((products?.length ?? 0) == 0) {
                    return;
                  }
                  Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (_) => OrderScreen(),
                        settings: RouteSettings(name: 'OrderScreen')),
                  );
                },
                child: Text(
                  AppLocalizations.of(context).translate('continue'),
                  style: TextStyle(color: Colors.white, fontSize: 18.0),
                ),
              ),
            ),
            Positioned(
              right: 8.0,
              child: Container(
                width: 100.0,
                height: 60.0,
                child: Center(
                  child: Container(
                    padding: const EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                      color: Theme.of(context).highlightColor,
                      borderRadius: BorderRadius.all(
                        Radius.circular(8.0),
                      ),
                    ),
                    child: FittedBox(
                      child: Text(
                        '${price.toStringAsFixed(2)} грн',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        )
      ],
    );
  }

  Widget _getContentWidget(List<CartProduct> cartProducts) {
    if (products == null || products.length < cartProducts.length) {
      return _getFutureCartProductsList(cartProducts);
    } else {
      return _getContent(_getCartProductsList(cartProducts));
    }
  }

  Widget _getCartProductsList(List<CartProduct> cartProducts) {
    return ListView.builder(
      itemCount: products.length,
      itemBuilder: (context, index) {
        Product product = products[index];
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: Row(
            children: <Widget>[
              GestureDetector(
                onTap: () => _showProduct(cartProducts[index]),
                child: Container(
                  width: 40.0,
                  height: 40.0,
                  child: ClipRRect(
                    child: Image.network(
                      product.imageUrl,
                      width: double.infinity,
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  ),
                ),
              ),
              SizedBox(
                width: 16.0,
              ),
              Expanded(
                child: Stack(
                  children: [
                    GestureDetector(
                      onTap: () => _showProduct(cartProducts[index]),
                      child: Container(
                        height: 60.0,
                        alignment: Alignment.centerLeft,
                        child: Text(
                          product.title,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    Positioned(
                      right: 0.0,
                      bottom: 0.0,
                      child: Container(
                        height: 20.0,
//                              color: Colors.blue,
                        child: FlatButton.icon(
                          icon: Icon(
                            Icons.delete,
                            color: Colors.red,
                            size: 12.0,
                          ),
                          onPressed: () =>
                              _showRemoveDialog(product, cartProducts, index),
                          label: Text(
                            'Delete',
                            style: TextStyle(fontSize: 10.0, color: Colors.red),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: 120.0,
                child: Counter(
                  initValue: product.measurementStep,
                  currentValue: cartProducts[index].quantity,
                  valueChanged: (quantity) {
                    StoreProvider.of<AppState>(context).dispatch(
                        ChangeCartProductQuantityAction(
                            cartProducts[index].productId, quantity));
                  },
                ),
              ),
              SizedBox(width: 8.0),
              Container(
                  width: 40.0,
                  child: FittedBox(
                    child: Text(
                      '${(cartProducts[index].quantity * product.price).toStringAsFixed(2)} грн',
                    ),
                  )),
            ],
          ),
        );
      },
    );
  }

  _showProduct(CartProduct cartProduct) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => ProductDetailsScreen(
          shopId: cartProduct.shopId,
          productId: cartProduct.productId,
          displayCart: false,
        ),
      ),
    );
  }

  _removeProduct(Product product, List<CartProduct> cartProducts, int index) {
    products.removeWhere((element) => element.id == product.id);
    StoreProvider.of<AppState>(context).dispatch(
        RemoveProductFromCartAction(cartProducts[index].shopId, product.id));
  }

  _showRemoveDialog(
      Product product, List<CartProduct> cartProducts, int index) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text(AppLocalizations.of(context).translate('continue_delete')),
              actions: [
                FlatButton(
                    onPressed: () {
                      _removeProduct(product, cartProducts, index);
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context).translate('yes'),
                      style: TextStyle(color: Colors.red),
                    )),
                FlatButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: Text(
                      AppLocalizations.of(context).translate('no')),
                    ),
              ],
            ));
  }

  Widget _getFutureCartProductsList(List<CartProduct> cartProducts) {
    return FutureBuilder(
      builder: (context, projectSnap) {
        if (projectSnap.connectionState == ConnectionState.done &&
            projectSnap.data != null) {
          products = projectSnap.data;
          _countPrice(cartProducts);
          return _getContent(_getCartProductsList(cartProducts));
        }
        return _getContent(Container());
      },
      future: _getProducts(cartProducts),
    );
  }

  Future _getProducts(List<CartProduct> cartProducts) async {
    List<Product> products = [];
    for (var cartProduct in cartProducts) {
      final documentSnapshot = await FirebaseFirestore.instance
          .collection(SHOPS_LINK)
          .doc(cartProduct.shopId)
          .collection('products')
          .doc(cartProduct.productId)
          .get();
      var productJson = documentSnapshot.data();
      productJson['id'] = documentSnapshot.id;
      final imageReference = FirebaseStorage().ref().child('sandbox').child(
          'storesProductsImages/${cartProduct.shopId}/${productJson['image']}');
      productJson['imageUrl'] = await imageReference.getDownloadURL();
      Product product = Product.fromJson(productJson);
      products.add(product);
    }
    return products;
  }
}
