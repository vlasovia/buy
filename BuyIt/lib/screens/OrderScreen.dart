import 'dart:async';
import 'dart:convert';

import 'package:BuyIt/model/UserData.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:BuyIt/model/AppState.dart';
import 'package:BuyIt/redux/actions.dart';
import 'package:BuyIt/screens/AddressScreen.dart';
import 'package:http/http.dart' as http;
import 'package:BuyIt/extensions.dart';
import 'package:BuyIt/AppLocalizations.dart';

import '../Common.dart';
import 'CommentScreen.dart';
import 'MainScreen.dart';
import 'PhoneLoginScreen.dart';

class OrderScreen extends StatefulWidget {
  final String shopId;
  final String productId;
  final double quantity;

  const OrderScreen({Key key, this.shopId, this.productId, this.quantity})
      : super(key: key);

  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  final _formKey = GlobalKey<FormState>();
  String _phoneNumber;
  String _firstName;
  String _lastName;
  String _selectedDeliveryValue;
  String _address;
  String _comment;
  bool _addressError = false;

  @override
  void initState() {
    _selectedDeliveryValue = MainScreen.deliveryTypes[0];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var address = StoreProvider.of<AppState>(context).state.userData?.address;
    if (address == null) {
      address = _address ??
          AppLocalizations.of(context).translate('enter_delivery_address');
    } else if (_address == null) {
      _address = address;
    } else {
      address = _address;
    }
//    if (_address != null) {
//      address = _address;
//    }
    final userData = StoreProvider.of<AppState>(context).state.userData;
    var phoneNumber = FirebaseAuth.instance.currentUser?.phoneNumber;
    if (phoneNumber == '') phoneNumber = null;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          AppLocalizations.of(context).translate('add_order'),
        ),
      ).withBottomAdmobBanner(context),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Form(
              key: _formKey,
              child: ListView(
                children: [
                  TextFormField(
                    decoration: InputDecoration(
                      labelText:
                          AppLocalizations.of(context).translate('person_name'),
                    ),
                    initialValue: userData?.firstName ?? '',
                    onSaved: (String value) {
                      _firstName = value;
                    },
                    validator: (String value) {
                      return value.isEmpty
                          ? AppLocalizations.of(context)
                              .translate('enter_person_name')
                          : null;
                    },
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: AppLocalizations.of(context)
                          .translate('person_second_name'),
                    ),
                    initialValue: userData?.lastName ?? '',
                    onSaved: (String value) {
                      _lastName = value;
                    },
                    validator: (String value) {
                      return value.isEmpty
                          ? AppLocalizations.of(context)
                              .translate('enter_person_second_name')
                          : null;
                    },
                  ),
                  TextFormField(
                    initialValue:
                        phoneNumber?.substring(4, phoneNumber.length) ?? '',
                    maxLength: 9,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      prefixText: '+380',
                      labelText: AppLocalizations.of(context)
                          .translate('mobile_phone_number'),
                    ),
                    onSaved: (String value) {
                      _phoneNumber = value;
                    },
                    validator: (String value) => value.length < 9
                        ? AppLocalizations.of(context)
                            .translate('add_phone_number')
                        : null,
                  ),
                  DropdownButton(
                    isExpanded: true,
                    value: _selectedDeliveryValue,
                    items: MainScreen.deliveryTypes
                        .map((e) => DropdownMenuItem(
                              value: e,
                              child: Text(
                                e,
                                textAlign: TextAlign.right,
                              ),
                            ))
                        .toList(),
                    onChanged: (newValue) => setState(() {
                      _selectedDeliveryValue = newValue;
                    }),
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  if (_selectedDeliveryValue == MainScreen.deliveryTypes[0])
                    GestureDetector(
                      onTap: () => Navigator.of(context)
                          .push(MaterialPageRoute(
                              builder: (_) => AddressScreen(
                                    address: address ==
                                            AppLocalizations.of(context)
                                                .translate(
                                                    'enter_delivery_address')
                                        ? ''
                                        : address,
                                  )))
                          .then((value) => setState(() {
                                _address = value == '' ? null : value;
                              })),
                      child: Text(
                        address,
//                        _address ?? _defaultAddressText,
                        maxLines: 2,
                        overflow: TextOverflow.fade,
                        style: TextStyle(
                            color: _addressError ? Colors.red : Colors.black),
                      ),
                    ),
                  SizedBox(
                    height: 16.0,
                  ),
                  GestureDetector(
                    onTap: () => Navigator.of(context)
                        .push(MaterialPageRoute(
                            builder: (_) => CommentScreen(
                                  comment: _comment,
                                )))
                        .then((value) => setState(() {
                              _comment = value == '' ? null : value;
                            })),
                    child: Text(
                      _comment ??
                          AppLocalizations.of(context)
                              .translate('want_add_comment'),
                      maxLines: 2,
                      overflow: TextOverflow.fade,
                    ),
                  ),
                  SizedBox(
                    height: 24.0,
                  ),
                  Container(
                    width: double.infinity,
                    height: 60.0,
                    decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.all(
                        Radius.circular(8.0),
                      ),
                    ),
                    child: FlatButton(
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            if (_selectedDeliveryValue == MainScreen.deliveryTypes[0] &&
                                    (_address?.isEmpty ?? true) ||
                                _address ==
                                    AppLocalizations.of(context)
                                        .translate('enter_delivery_address')) {
                              setState(() {
                                _addressError = true;
                              });
                              return;
                            }
                            _formKey.currentState.save();
                            _showConfirmationDialog(context);
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context).translate('continue'),
                          style: TextStyle(color: Colors.white, fontSize: 18.0),
                        )),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _showConfirmationDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (_) => AlertDialog(
              content:
                  Text(AppLocalizations.of(context).translate('add_new_order')),
              actions: <Widget>[
                FlatButton(
                  child: Text(AppLocalizations.of(context).translate('yes'),
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  onPressed: () {
                    Navigator.of(context).pop();
                    if (FirebaseAuth.instance.currentUser.isAnonymous) {
                      _moveToLogin();
                    } else if (FirebaseAuth.instance.currentUser.phoneNumber !=
                        '+380' + _phoneNumber) {
                      _showNewUserDialog();
                    } else {
                      _sendOrder();
                    }
                  },
                ),
                FlatButton(
                  child: Text(AppLocalizations.of(context).translate('no'),
                      style: TextStyle(color: Colors.grey)),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            ));
  }

  sendNewOrderNotification(Map<String, String> ordersIds) {
    ordersIds.forEach((shopId, orderId) async {
      final querySnapshotShop = await FirebaseFirestore.instance
          .collection(SHOPS_LINK)
          .doc(shopId)
          .get();
      final ownerId = querySnapshotShop.data()["owner"];

      final querySnapshotUser = await FirebaseFirestore.instance
          .collection(USERS_LINK)
          .doc(ownerId)
          .get();
      final tokens = querySnapshotUser.data()["tokens"];
      tokens.forEach((token) => sendNotification(
          token as String,
          AppLocalizations.of(context).translate('new_order_push'),
          AppLocalizations.of(context).translate('new_order'),
          shopId,
          orderId));
      // final token = querySnapshotUser.data()["token"];
      // sendNotification(token, 'Пользователь сделал новый заказ', 'Новый заказ', shopId, orderId);
      // sendNotification(token, 'Пользователь сделал новый заказ', 'Новый заказ',
      //     {'open_order': {shopId: orderId}});
    });
  }

  _updateUserData() {
    StoreProvider.of(context).dispatch(UpdateUserDataAction(UserData(
        firstName: _firstName, lastName: _lastName, address: _address)));
  }

  _showNewUserDialog() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(
          '${AppLocalizations.of(context).translate('different_mobile_number_warning')} +380$_phoneNumber?',
        ),
        actions: [
          FlatButton(
            child: Text(
              AppLocalizations.of(context).translate('cancel'),
              style: TextStyle(color: Colors.red),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          FlatButton(
              child: Text(AppLocalizations.of(context).translate('yes')),
              onPressed: () {
                Navigator.of(context).pop();
                _moveToLogin();
              }),
        ],
      ),
    );
  }

  _moveToLogin() {
    Navigator.of(context)
        .push(
      MaterialPageRoute(
        builder: (context) => PhoneLoginScreen(
          moveToMainScreen: false,
          phoneNumber: _phoneNumber,
        ),
      ),
    )
        .then(
      (value) {
        if (value['verified'] ?? false == true) {
          _sendOrder();
        }
      },
    );
  }

  _sendOrder() {
    final overlayEntry = showProgressIndicator(context);
    StoreProvider.of<AppState>(context).dispatch(
      AddOrderAction(
        context: context,
        firstName: _firstName,
        lastName: _lastName,
        delivery: MainScreen.deliveryTypes.indexOf(_selectedDeliveryValue),
        address: _address,
        comment: _comment,
        response: (data, error) {
          overlayEntry?.remove();
          if (error == null) {
            sendNewOrderNotification(data);
            showDialog(
                context: context,
                builder: (_) => new AlertDialog(
                      title: Text(
                          AppLocalizations.of(context).translate('new_order')),
                      content: Text(AppLocalizations.of(context)
                          .translate('order_being_accepted')),
                      actions: <Widget>[
                        FlatButton(
                          child: Text(
                              AppLocalizations.of(context).translate('close')),
                          onPressed: () {
                            Navigator.of(context).popUntil(
                                (route) => route.settings.name == '/');
                          },
                        ),
                      ],
                    ));
          } else {}
        },
      ),
    );
  }
}
