import 'package:flutter/material.dart';
import 'package:BuyIt/extensions.dart';
import 'package:BuyIt/AppLocalizations.dart';

class AddressScreen extends StatefulWidget {

  final String address;

  const AddressScreen({Key key, this.address}) : super(key: key);

  @override
  _AddressScreenState createState() => _AddressScreenState();
}

class _AddressScreenState extends State<AddressScreen> {

  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    if (widget.address != null) {
      _controller.text = widget.address;
    }
    super.initState();
  }

  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  _done() {
    Navigator.pop(context, _controller.text);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          AppLocalizations.of(context).translate('enter_address'),
        ),
      ).withBottomAdmobBanner(context),
      body: WillPopScope(
        onWillPop: () {
          _done();
          return new Future(() => false);
        },
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                TextField(minLines: 5, maxLines: 5, controller: _controller,),
                SizedBox(height: 16.0,),
                Container(
                  width: double.infinity,
                  height: 60.0,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.all(
                      Radius.circular(8.0),
                    ),
                  ),
                  child: FlatButton(
                      onPressed: _done,
                      child: Text(
                        'Готово',
                        style: TextStyle(color: Colors.white, fontSize: 18.0),
                      )),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
