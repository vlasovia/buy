import 'dart:io';
import 'dart:ui';

import 'package:BuyIt/model/AppState.dart';
import 'package:BuyIt/model/Shop.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:image_picker/image_picker.dart';
import 'package:uuid/uuid.dart';
import 'package:path/path.dart' as p;
import 'package:BuyIt/extensions.dart';

import 'package:BuyIt/AppLocalizations.dart';

import '../Common.dart';

class ShopEditScreen extends StatefulWidget {
  final Shop shop;
  final Locale myLocale;

  const ShopEditScreen({Key key, this.shop, this.myLocale}) : super(key: key);

  @override
  _ShopEditScreenState createState() => _ShopEditScreenState();
}

class _ShopEditScreenState extends State<ShopEditScreen> {
  final _formKey = GlobalKey<FormState>();

  final ImagePicker _picker = ImagePicker();
  PickedFile _imageFile;
  dynamic _pickImageError;
  String _retrieveDataError;

  final picker = ImagePicker();

  String _name;
  String _description;
  String _phone;
  String _address;

  List<String> languages = ['ru', 'uk'];
  String selectedLanguage;

  @override
  void initState() {
    selectedLanguage = widget.myLocale.languageCode;
    super.initState();
  }

//  Future getImage() async {
//    final pickedFile = await picker.getImage(source: ImageSource.camera);
//
//    setState(() {
//      _imageFile = File(pickedFile.path);
//    });
//  }

  Future<String> _uploadFile() async {
    final String uuid = Uuid().v1();
    final fileName = '$uuid${p.extension(_imageFile.path)}';
    final StorageReference ref = FirebaseStorage()
        .ref()
        .child('sandbox')
        .child('storesTitlesImages/${widget.shop.id}/$fileName');
    final StorageUploadTask uploadTask = ref.putFile(
      File(_imageFile.path),
    );
    await uploadTask.onComplete;
    return fileName;
  }

  Text _getRetrieveErrorWidget() {
    if (_retrieveDataError != null) {
      final Text result = Text(_retrieveDataError);
      _retrieveDataError = null;
      return result;
    }
    return null;
  }

  Widget _previewImage() {
    final Text retrieveError = _getRetrieveErrorWidget();
    if (retrieveError != null) {
      return retrieveError;
    }
    if (_imageFile != null) {
      if (kIsWeb) {
        // Why network?
        // See https://pub.dev/packages/image_picker#getting-ready-for-the-web-platform
        return Image.network(_imageFile.path);
      } else {
        return Image.file(File(_imageFile.path));
      }
    } else if (_pickImageError != null) {
      return Text(
        'Pick image error: $_pickImageError',
        textAlign: TextAlign.center,
      );
    } else if (widget.shop.titleImageUrl != null) {
      return Image.network(widget.shop.titleImageUrl);
    } else {
      return const Text(
        'You have not yet picked an image.',
        textAlign: TextAlign.center,
      );
    }
  }

  Future<void> retrieveLostData() async {
    final LostData response = await _picker.getLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
      setState(() {
        _imageFile = response.file;
      });
    } else {
      _retrieveDataError = response.exception.code;
    }
  }

  Future<void> _setProduct(OverlayEntry overlayEntry, {String fileName}) async {
    var productJson = {
      'name': _name,
      'description': _description,
      'phone': _phone,
      'address': _address,
    };
    if (fileName != null) {
      productJson['titleImage'] = fileName;
    }
    await FirebaseFirestore.instance
        .collection(SHOPS_LINK)
        .doc(widget.shop.id)
        .set(productJson, SetOptions(merge: true));
    overlayEntry.remove();
    Navigator.of(context).pop({'updated': true});
  }

  _onPressedSetButton() {
    if (_formKey.currentState.validate()) {
      final overlayEntry = showProgressIndicator(context);
      _formKey.currentState.save();
      // _imageFile == null ? _setProduct(overlayEntry) : _uploadFile().then((fileName) => _setProduct(overlayEntry, fileName: fileName));
    }
  }

  @override
  Widget build(BuildContext context) {
    final shop = widget.shop;
    return StoreConnector<AppState, User>(
      converter: (store) => store.state.user,
      builder: (context, user) {
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            centerTitle: true,
            title: Text(
              '${AppLocalizations.of(context).translate('edit')} ${shop.name}',
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.check),
                color: Colors.black38,
                onPressed: _onPressedSetButton,
              ),
            ],
          ).withBottomAdmobBanner(context),
          body: SafeArea(
            child: GestureDetector(
              onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: SingleChildScrollView(
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        Center(
                          child: _previewImage(),
                        ),
                        // Row(
                        //   // mainAxisSize: MainAxisSize.min,
                        //   mainAxisAlignment: MainAxisAlignment.end,
                        //   children: [
                        //     Text(
                        //       AppLocalizations.of(context).translate('lang'),
                        //     ),
                        //     SizedBox(width: 8.0),
                        //     DropdownButton<String>(
                        //       value: selectedLanguage,
                        //       // icon: Icon(Icons.arrow_downward),
                        //       // iconSize: 24,
                        //       // elevation: 16,
                        //       // style: TextStyle(color: Colors.deepPurple),
                        //       // underline: Container(
                        //       //   height: 2,
                        //       //   color: Colors.deepPurpleAccent,
                        //       // ),
                        //       onChanged: (String newValue) {
                        //         setState(() {
                        //           selectedLanguage = newValue;
                        //         });
                        //       },
                        //       items: languages.map<DropdownMenuItem<String>>(
                        //           (String value) {
                        //         return DropdownMenuItem<String>(
                        //           value: value,
                        //           child: Text(
                        //               value == 'ru' ? 'Русский' : 'Українська'),
                        //         );
                        //       }).toList(),
                        //     ),
                        //   ],
                        // ),
                        TextFormField(
                          maxLines: 1,
                          initialValue: shop.name ?? '',
                          decoration: InputDecoration(
                              hintText: AppLocalizations.of(context)
                                  .translate('name')),
                          onSaved: (value) => _name = value,
                          validator: (value) => value.isEmpty
                              ? AppLocalizations.of(context)
                                  .translate('enter_name')
                              : null,
                        ),
                        TextFormField(
                          maxLines: 4,
                          initialValue: shop.description ?? '',
                          decoration: InputDecoration(
                              hintText: AppLocalizations.of(context)
                                  .translate('description')),
                          onSaved: (value) => _description = value,
                          validator: (value) => value.isEmpty
                              ? AppLocalizations.of(context)
                                  .translate('enter_description')
                              : null,
                        ),
                        TextFormField(
                          maxLength: 9,
                          initialValue: shop?.phone == null
                              ? ''
                              : shop.phone.substring(4),
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            prefixText: "+380",
                            labelText: AppLocalizations.of(context).translate('phone_number'),
                          ),
                          onSaved: (value) => _phone = "+380$value",
                          validator: (String value) => value.length < 9
                              ? AppLocalizations.of(context).translate('add_phone_number')
                              : null,
                        ),
                        TextFormField(
                          maxLines: 4,
                          initialValue: shop.address ?? '',
                          decoration: InputDecoration(hintText: AppLocalizations.of(context).translate('address')),
                          onSaved: (value) => _address = value,
                          validator: (value) =>
                              value.isEmpty ? AppLocalizations.of(context).translate('enter_address') : null,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          floatingActionButton: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FloatingActionButton(
                onPressed: () {
                  _onImageButtonPressed(ImageSource.gallery, context: context);
                },
                heroTag: 'image0',
                tooltip: 'Pick Image from gallery',
                child: const Icon(
                  Icons.photo_library,
                  color: Colors.white,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: FloatingActionButton(
                  onPressed: () {
                    _onImageButtonPressed(ImageSource.camera, context: context);
                  },
                  heroTag: 'image1',
                  tooltip: 'Take a Photo',
                  child: const Icon(
                    Icons.camera_alt,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void _onImageButtonPressed(ImageSource source, {BuildContext context}) async {
    try {
      final pickedFile = await _picker.getImage(
        source: source,
        maxWidth: 1024.0,
        maxHeight: 1024.0,
        imageQuality: 80,
      );
      setState(() {
        _imageFile = pickedFile;
      });
    } catch (e) {
      setState(() {
        _pickImageError = e;
      });
    }
  }
}
