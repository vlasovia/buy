import 'dart:io';
import 'dart:ui';

import 'package:BuyIt/Common.dart';
import 'package:BuyIt/model/UserData.dart';
import 'package:BuyIt/screens/EditProductScreen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:BuyIt/model/AppState.dart';
import 'package:BuyIt/model/CartProduct.dart';
import 'package:BuyIt/model/Product.dart';
import 'package:BuyIt/redux/actions.dart';
import 'package:BuyIt/widgets/CartButton.dart';
import 'package:BuyIt/widgets/Counter.dart';
import 'package:BuyIt/extensions.dart';
import 'package:full_screen_image/full_screen_image.dart';
import 'package:BuyIt/AppLocalizations.dart';

import 'CartScreen.dart';

class ProductDetailsScreen extends StatefulWidget {
  final String shopId;
  final String productId;
  final displayCart;

  const ProductDetailsScreen(
      {Key key, this.shopId, this.productId, this.displayCart = true})
      : super(key: key);

  @override
  _ProductDetailsScreenState createState() => _ProductDetailsScreenState();
}

class _ProductDetailsScreenState extends State<ProductDetailsScreen> {
  double productQuantity = 0;

  _showCart() {
    if (widget.displayCart) {
      Navigator.of(context).push(
        MaterialPageRoute(builder: (_) => CartScreen()),
      );
    } else {
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, Product>(
      converter: (store) => store.state.shops
          .firstWhere((shop) => shop.id == widget.shopId)
          .products
          ?.firstWhere((product) => product.id == widget.productId),
      builder: (context, product) {
        if (product == null) {
          return FutureBuilder<Product>(
//                  future: getShopOrders('qKUby69ZUjtJNwkkgZqO'),
            future: _getDocument(),
            builder: (context, AsyncSnapshot<Product> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Scaffold(
                  appBar: AppBar(
                    elevation: 0,
                    backgroundColor: Colors.transparent,
                    centerTitle: true,
                    title: Text(
                      '',
                    ),
                    actions: <Widget>[
                      if (widget.displayCart) CartButton(),
                    ],
                  ).withBottomAdmobBanner(context),
                  body: SafeArea(
                    child: Center(
                      child: Text(AppLocalizations.of(context).translate('loading') + '...'),
                    ),
                  ),
                );
              } else {
                return _screenWidget(snapshot.data);
              }
            },
          );
        } else {
          return _screenWidget(product);
        }
      },
    );
  }

  Widget _screenWidget(Product product) {
    productQuantity = product.measurementStep;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          product.title,
        ),
        actions: <Widget>[
          if (widget.displayCart) CartButton(),
        ],
      ).withBottomAdmobBanner(context),
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    height: 200.0,
                    child: Card(
                      margin: const EdgeInsets.all(16.0),
                      elevation: 5,
                      child: Stack(
                        children: [
                          FullScreenWidget(
                            child: Hero(
                              tag: 'someProdImage',
                              child: ClipRRect(
                                child: Image.network(
                                  product.imageUrl,
                                  width: double.infinity,
                                  fit: BoxFit.cover,
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(16.0)),
                              ),
                            ),
                          ),
                        ],
                      ),
                      shape: RoundedRectangleBorder(
                        side: BorderSide(
                          color: Theme.of(context).primaryColor,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(16.0)),
                      ),
                    ),
                  ),
                  Text(
                    product.description.replaceAll("\\n", "\n"),
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          '₴${product.price} /${product.measurement}',
                          textAlign: TextAlign.right,
                          style:
                              TextStyle(color: Theme.of(context).primaryColor),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  _cartButtons(product),
                  StoreConnector<AppState, UserData>(
                    converter: (store) => store.state.userData,
                    builder: (context, userData) {
                      if (userData?.myShopId == null) {
                        return Container();
                      } else {
                        return FlatButton(
                          child: Text(AppLocalizations.of(context).translate('edit')),
                          onPressed: () => Navigator.of(context)
                              .push(
                            MaterialPageRoute(
                              builder: (context) => EditProductScreen(
                                shopId: widget.shopId,
                                product: product,
                              ),
                            ),
                          )
                              .then((value) {
                            if (value is Map && value['updated'] != null) {
                              StoreProvider.of<AppState>(context).dispatch(
                                  LoadShopProductsAction(
                                      context, widget.shopId));
                            }
                          }),
                        );
                      }
                    },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _cartButtons(Product product) {
    final greyColor = Colors.black12;
    final decoration = BoxDecoration(
        border: Border.all(color: greyColor),
        borderRadius: BorderRadius.all(Radius.circular(8.0)));
    return StoreConnector<AppState, List<CartProduct>>(
      converter: (store) => store.state.cartProducts,
      builder: (context, cartProducts) {
        CartProduct cartProduct;
        if (cartProducts != null) {
          cartProduct = cartProducts.firstWhere(
              (cartProduct) => cartProduct.productId == product.id,
              orElse: () => null);
        }
        final productInactive = product.status != ProductStatus.Active;
        final buttonActive =
            cartProduct == null && product.status == ProductStatus.Active;
        final backgroundColor =
            buttonActive ? Theme.of(context).primaryColor : greyColor;
        final textColor = buttonActive ? Colors.white : Colors.black26;
        return Row(
          children: [
            Expanded(
              child: Container(
                margin: const EdgeInsets.all(8.0),
                decoration: decoration,
                child: Counter(
                  initValue: buttonActive ? product.measurementStep : 0,
                  currentValue: buttonActive
                      ? product.measurementStep
                      : productInactive
                          ? 0
                          : cartProduct.quantity,
                  valueChanged: (value) {
                    if (buttonActive) {
                      productQuantity = value;
                    }
                  },
                ),
              ),
            ),
            Expanded(
              child: Container(
                margin: const EdgeInsets.all(8.0),
                decoration: decoration.copyWith(
                  color: backgroundColor,
                ),
                child: FlatButton(
                  child: Text(
                    buttonActive
                        ? AppLocalizations.of(context).translate('add_to_cart')
                        : productInactive
                            ? AppLocalizations.of(context).translate('not_available')
                            : AppLocalizations.of(context).translate('is_in_cart'),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: textColor,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onPressed: () {
                    if (buttonActive) {
                      StoreProvider.of<AppState>(context).dispatch(
                        AddProductToCartAction(
                          widget.shopId,
                          product.id,
                          product.title,
                          product.description,
                          product.price,
                          productQuantity,
                        ),
                      );
                    } else if (!productInactive) {
                      _showCart();
                    }
                  },
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Future<Product> _getDocument() async {
    final productSnapshot = await FirebaseFirestore.instance
        .collection(SHOPS_LINK)
        .doc(widget.shopId)
        .collection('products')
        .doc(widget.productId)
        .get();
    var productJson = productSnapshot.data();
    productJson['id'] = widget.productId;
    final imageReference = FirebaseStorage()
        .ref()
        .child('sandbox')
        .child('storesProductsImages/${widget.shopId}/${productJson['image']}');
    productJson['imageUrl'] = await imageReference.getDownloadURL();
    Product product = Product.fromJson(productJson);
    return product;
  }
}

typedef void OnPickImageCallback(
    double maxWidth, double maxHeight, int quality);
