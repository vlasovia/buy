import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:BuyIt/model/AppState.dart';
import 'package:BuyIt/model/ProductType.dart';
import 'package:BuyIt/model/Shop.dart';
import 'package:BuyIt/extensions.dart';

class SelectProductTypeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, User>(
      converter: (store) => store.state.user,
      builder: (context, user) {
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            centerTitle: true,
            title: Text(
              'Choose Product Type',
            ),
          ).withBottomAdmobBanner(context),
          body: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: StoreConnector<AppState, List<ProductType>>(
                converter: (store) => store.state.productTypes,
                builder: (context, productTypes) {
                      return ListView(
                        children: productTypes
                            .map((productType) {
                          return ListTile(
                            title: RaisedButton(
                              child: Text(productType.ru),
                              onPressed: () => Navigator.pop(context, {
                                'id': productType.id,
                                'name': productType.ru
                              }),
                            ),
                          );
                        }).toList(),
                      );
                }
              ),
            ),
          ),
//          body: SafeArea(
//            child: Padding(
//              padding: const EdgeInsets.all(16.0),
//              child: StreamBuilder<QuerySnapshot>(
//                stream: Firestore.instance
//                    .collection('product_types_test')
//                    .orderBy('en')
//                    .snapshots(),
//                builder: (BuildContext context,
//                    AsyncSnapshot<QuerySnapshot> snapshot) {
//                  if (snapshot.hasError)
//                    return Text('Error: ${snapshot.error}');
//                  switch (snapshot.connectionState) {
//                    case ConnectionState.waiting:
//                      return Text('Loading...');
//                    default:
//                      return ListView(
//                        children: snapshot.data.documents
//                            .map((DocumentSnapshot document) {
//                          return ListTile(
//                            title: RaisedButton(
//                              child: Text(document['ru']),
//                              onPressed: () => Navigator.pop(context, {
//                                'id': document.documentID,
//                                'name': document['ru']
//                              }),
//                            ),
//                          );
//                        }).toList(),
//                      );
//                  }
//                },
//              ),
//            ),
//          ),
        );
      },
    );
  }

  List<Widget> _getShopsWidgets(BuildContext context, List<Shop> shops) {
    if (shops == null) return [];
    final borderRadius = BorderRadius.all(Radius.circular(16.0));
    return shops
        .map(
          (shop) => Container(
            height: 200.0,
            child: Card(
              margin: const EdgeInsets.all(16.0),
//              margin: const EdgeInsets.symmetric(vertical: 16.0),
              elevation: 5,
//            color: backgroundColor,
              child: Stack(
                children: [
                  ClipRRect(
                    child: Image.network(
                      shop.titleImageUrl,
                      width: double.infinity,
                      fit: BoxFit.cover,
                    ),
                    borderRadius: borderRadius,
                  ),
                  Container(
                    width: double.infinity,
                    height: double.infinity,
                    child: Center(
                      child: Text(
                        shop.name,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 40.0,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              shape: RoundedRectangleBorder(
                side: BorderSide(
                  color: Theme.of(context).primaryColor,
                ),
                borderRadius: borderRadius,
              ),
            ),
          ),
        )
        .toList();
  }
}
