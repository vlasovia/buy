import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:BuyIt/model/AppState.dart';
import 'package:BuyIt/redux/actions.dart';
import 'package:BuyIt/screens/PhoneLoginScreen.dart';

import '../AppLocalizations.dart';
import 'MainScreen.dart';
import '../Common.dart';
import 'package:BuyIt/extensions.dart';

class LoginScreen extends StatelessWidget {

  Future<void> _handleSignIn(BuildContext context) async {
//      final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
//      final GoogleSignInAuthentication googleAuth =
//          await googleUser.authentication;
//
//      final AuthCredential credential = GoogleAuthProvider.getCredential(
//        accessToken: googleAuth.accessToken,
//        idToken: googleAuth.idToken,
//      );
//
//      final FirebaseUser user =
//          (await _auth.signInWithCredential(credential)).user;
//      print("signed in " + user.displayName);
//      return user;
    final overlayEntry = showProgressIndicator(context);
    StoreProvider.of<AppState>(context).dispatch(
      GoogleLogInAction(
        context,
        '',
        '',
        () {
          overlayEntry.remove();
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              settings: RouteSettings(name: '/'),
              builder: (_) => MainScreen(),
//                  firstScreen == FirstScreen.intro ? Intro52() : MainScreen(),
            ),
          );
        },
        () => overlayEntry.remove(),
      ),
    );
  }

  _testLogin(BuildContext context) async {
    final overlayEntry = showProgressIndicator(context);
    try {
      final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;
      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      final User user =
          (await FirebaseAuth.instance.signInWithCredential(credential)).user;
      print("signed in " + user.displayName);
//      return user;
    } catch (error) {
      print(error);
//      return null;
    }
    overlayEntry.remove();
  }

  @override
  Widget build(BuildContext context) {
    Widget _signInButton(String loginProvider, VoidCallback callback) {
      return OutlineButton(
        splashColor: Theme.of(context).primaryColor,
        onPressed: callback,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
        highlightElevation: 0,
        borderSide: BorderSide(color: Colors.grey),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Image(
                  image: AssetImage(
                      "images/${loginProvider.toLowerCase()}_logo.png"),
                  height: 25.0),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  loginProvider,
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.grey,
                  ),
                ),
              )
            ],
          ),
        ),
      );
    }

    return Scaffold(
        appBar: AppBar(
          title: Text('Login'),
        ).withBottomAdmobBanner(context),
        body: SafeArea(
          child: Center(
            child: Container(
              width: 200.0,
              child: Column(
                children: [
                  _signInButton('Phone', () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (_) => PhoneLoginScreen()));
//                    _phoneSignIn();
                  }),
                  _signInButton('Google', () {
                    _handleSignIn(context);
                  }),
                  _signInButton('Facebook', () {}),
                  FlatButton(
                    onPressed: () {
                      _testLogin(context);
                    },
                    child: Text(
                      'asdasd',
                      //AppLocalizations.of(context).translate('companyLogin'),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  _phoneSignIn() async {
    await FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: '+380952067661',
      verificationCompleted: (PhoneAuthCredential credential) {

      },
      verificationFailed: (FirebaseAuthException e) {

      },
      codeSent: (String verificationId, int resendToken) {

      },
      codeAutoRetrievalTimeout: (String verificationId) {

      },
    );
  }
//
//    return Scaffold(
//        body: SafeArea(
//      child: Center(
//        child: Container(
//          width: 300,
//          child: Column(
//            children: [
//              _signInButton('Google'),
//              _signInButton('Facebook'),
//              FlatButton(
//                onPressed: () {},
//                child: Text(
//                  AppLocalizations.of(context).translate('companyLogin'),
//                ),
//              ),
//            ],
//          ),
//        ),
//      ),
//    ));
//  }
}
