import 'dart:io';
import 'dart:ui';

import 'package:BuyIt/extensions.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:image_picker/image_picker.dart';
import 'package:BuyIt/model/AppState.dart';
import 'package:BuyIt/model/Shop.dart';
import 'package:BuyIt/screens/SelectProductTypeScreen.dart';
import 'package:uuid/uuid.dart';
import 'package:path/path.dart' as p;
import 'package:BuyIt/AppLocalizations.dart';

import '../Common.dart';

class AddProductScreen extends StatefulWidget {
  final Shop shop;

  const AddProductScreen({Key key, this.shop}) : super(key: key);

  @override
  _AddProductScreenState createState() => _AddProductScreenState();
}

class _AddProductScreenState extends State<AddProductScreen> {
  String productTypeId;
  String productTypeName;
  final ImagePicker _picker = ImagePicker();
  PickedFile _imageFile;
  dynamic _pickImageError;
  String _retrieveDataError;

  final picker = ImagePicker();
  final TextEditingController maxWidthController = TextEditingController();
  final TextEditingController maxHeightController = TextEditingController();
  final TextEditingController qualityController = TextEditingController();

  final titleTextEditingController = TextEditingController();
  final descriptionTextEditingController = TextEditingController();
  final priceTextEditingController = TextEditingController();

  String dropdownValue = 'кг';
  String productStatusDropdownValue;

//  Future getImage() async {
//    final pickedFile = await picker.getImage(source: ImageSource.camera);
//
//    setState(() {
//      _imageFile = File(pickedFile.path);
//    });
//  }

  Future<String> _uploadFile() async {
    final String uuid = Uuid().v1();
    final fileName = '$uuid${p.extension(_imageFile.path)}';
    final StorageReference ref = FirebaseStorage()
        .ref()
        .child('sandbox')
        .child('storesProductsImages/${widget.shop.id}/$fileName');
    final StorageUploadTask uploadTask = ref.putFile(
      File(_imageFile.path),
    );
    await uploadTask.onComplete;
    return fileName;
  }

  @override
  void dispose() {
    maxWidthController.dispose();
    maxHeightController.dispose();
    qualityController.dispose();
    titleTextEditingController.dispose();
    descriptionTextEditingController.dispose();
    priceTextEditingController.dispose();
    super.dispose();
  }

  Text _getRetrieveErrorWidget() {
    if (_retrieveDataError != null) {
      final Text result = Text(_retrieveDataError);
      _retrieveDataError = null;
      return result;
    }
    return null;
  }

  Widget _previewImage() {
    final Text retrieveError = _getRetrieveErrorWidget();
    if (retrieveError != null) {
      return retrieveError;
    }
    if (_imageFile != null) {
      if (kIsWeb) {
        // Why network?
        // See https://pub.dev/packages/image_picker#getting-ready-for-the-web-platform
        return Image.network(_imageFile.path);
      } else {
        return Image.file(File(_imageFile.path));
      }
    } else if (_pickImageError != null) {
      return Text(
        'Pick image error: $_pickImageError',
        textAlign: TextAlign.center,
      );
    } else {
      return const Text(
        'You have not yet picked an image.',
        textAlign: TextAlign.center,
      );
    }
  }

  Future<void> retrieveLostData() async {
    final LostData response = await _picker.getLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
      setState(() {
        _imageFile = response.file;
      });
    } else {
      _retrieveDataError = response.exception.code;
    }
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, User>(
      converter: (store) => store.state.user,
      builder: (context, user) {
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            centerTitle: true,
            title: Text(
              AppLocalizations.of(context).translate('new_product'),
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.add),
                color: Colors.black38,
                onPressed: () {
                  _uploadFile().then((fileName) {
                    FirebaseFirestore.instance
                        .collection(SHOPS_LINK)
                        .doc(widget.shop.id)
                        .collection('products')
                        .doc()
                        .set(
                      {
                        'type': FirebaseFirestore.instance.collection('product_types_test').doc(productTypeId),
                        'title': titleTextEditingController.text,
                        'description': descriptionTextEditingController.text,
                        'price': double.parse(priceTextEditingController.text),
                        'measurement': dropdownValue,
                        'image': fileName,
                      },
                    );
                  });
                },
              ),
            ],
          ).withBottomAdmobBanner(context),
          body: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  Center(
                    child: _previewImage(),
                  ),
                  TextField(
                    maxLines: 1,
                    decoration: InputDecoration(hintText: AppLocalizations.of(context).translate('name')),
                    controller: titleTextEditingController,
                  ),
                  TextField(
                    maxLines: 4,
                    decoration: InputDecoration(hintText: AppLocalizations.of(context).translate('description')),
                    controller: descriptionTextEditingController,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: 80.0,
                          child: TextField(
                            maxLines: 1,
                            decoration: InputDecoration(
                              hintText: AppLocalizations.of(context).translate('price'),
                              contentPadding: EdgeInsets.all(0.0),
                              isDense: true,
                            ),
                            controller: priceTextEditingController,
                            keyboardType:
                                TextInputType.numberWithOptions(decimal: true),
                          ),
                        ),
                        DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            value: dropdownValue,
//                        icon: Icon(Icons.arrow_downward),
//                        iconSize: 24,
//                        elevation: 16,
//                        style: TextStyle(color: Colors.deepPurple),
//                        underline: Container(
//                          height: 2,
//                          color: Colors.deepPurpleAccent,
//                        ),
                            onChanged: (String newValue) {
                              setState(() {
                                dropdownValue = newValue;
                              });
                            },
                            isDense: true,
                            underline: null,
//                        decoration: InputDecoration(
//                          hintText: 'Price',
//                          contentPadding: EdgeInsets.all(0.0),
//                        ),
                            items: <String>['кг', 'штука']
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: 80.0,
                          child: Text('Статус'),
                        ),
                        DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            value: productStatusDropdownValue ?? ProductStatus.Active.stringValue(context),
                            onChanged: (String newValue) {
                              setState(() {
                                productStatusDropdownValue = newValue;
                              });
                            },
                            isDense: true,
                            underline: null,
                            items: ProductStatus.values.map((ProductStatus value) {
                              return DropdownMenuItem<String>(
                                value: value.stringValue(context),
                                child: Text(value.stringValue(context)),
                              );
                            }).toList(),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          floatingActionButton: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FloatingActionButton(
                onPressed: () {
                  _onImageButtonPressed(ImageSource.gallery, context: context);
                },
                heroTag: 'image0',
                tooltip: 'Pick Image from gallery',
                child: const Icon(
                  Icons.photo_library,
                  color: Colors.white,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: FloatingActionButton(
                  onPressed: () {
                    _onImageButtonPressed(ImageSource.camera, context: context);
                  },
                  heroTag: 'image1',
                  tooltip: 'Take a Photo',
                  child: const Icon(
                    Icons.camera_alt,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void _onImageButtonPressed(ImageSource source, {BuildContext context}) async {
    await _displayPickImageDialog(
      context,
      (double maxWidth, double maxHeight, int quality) async {
        try {
          final pickedFile = await _picker.getImage(
            source: source,
            maxWidth: maxWidth,
            maxHeight: maxHeight,
            imageQuality: quality,
          );
          setState(() {
            _imageFile = pickedFile;
          });
        } catch (e) {
          setState(() {
            _pickImageError = e;
          });
        }
      },
    );
  }

  Future<void> _displayPickImageDialog(
      BuildContext context, OnPickImageCallback onPick) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Add optional parameters'),
            content: Column(
              children: <Widget>[
                TextField(
                  controller: maxWidthController,
                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                  decoration:
                      InputDecoration(hintText: "Enter maxWidth if desired"),
                ),
                TextField(
                  controller: maxHeightController,
                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                  decoration:
                      InputDecoration(hintText: "Enter maxHeight if desired"),
                ),
                TextField(
                  controller: qualityController,
                  keyboardType: TextInputType.number,
                  decoration:
                      InputDecoration(hintText: "Enter quality if desired"),
                ),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: const Text('CANCEL'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                  child: const Text('PICK'),
                  onPressed: () {
                    double width = maxWidthController.text.isNotEmpty
                        ? double.parse(maxWidthController.text)
                        : null;
                    double height = maxHeightController.text.isNotEmpty
                        ? double.parse(maxHeightController.text)
                        : null;
                    int quality = qualityController.text.isNotEmpty
                        ? int.parse(qualityController.text)
                        : null;
                    onPick(width, height, quality);
                    Navigator.of(context).pop();
                  }),
            ],
          );
        });
  }
}

typedef void OnPickImageCallback(
    double maxWidth, double maxHeight, int quality);
