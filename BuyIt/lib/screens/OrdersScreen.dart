import 'package:BuyIt/Common.dart';
import 'package:BuyIt/model/AppState.dart';
import 'package:BuyIt/model/CartProduct.dart';
import 'package:BuyIt/model/Order.dart';
import 'package:BuyIt/model/UserData.dart';
import 'package:BuyIt/redux/actions.dart';
import 'package:BuyIt/screens/PastOrderScreen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:BuyIt/extensions.dart';
import 'package:BuyIt/AppLocalizations.dart';

class OrdersScreen extends StatefulWidget {
  @override
  _OrdersScreenState createState() => _OrdersScreenState();
}

class _OrdersScreenState extends State<OrdersScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          AppLocalizations.of(context).translate('orders'),
        ),
      ).withBottomAdmobBanner(context),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: StoreConnector<AppState, UserData>(
              converter: (store) => store.state.userData,
              builder: (context, userData) {
                return StoreConnector<AppState, List<Order>>(
                    onInit: (store) => store.dispatch(LoadOrdersAction(
                        context: context, myShopId: userData.myShopId)),
                    converter: (store) => store.state.orders,
                    builder: (context, orders) {
                      return orders == null
                          ? Center(
                              child: Text(AppLocalizations.of(context)
                                  .translate('loading') + '...'),
                            )
                          : ListView.builder(
                              itemCount: orders.length,
                              itemBuilder: (context, index) {
                                return _orderWidget(orders[index]);
                              },
                            );
                      // return FutureBuilder<List<Order>>(
                      //   future: userData.myShopId == null ? getUserOrders() : getShopOrders(userData.myShopId),
                      //   builder: (context, AsyncSnapshot<List<Order>> snapshot) {
                      //     if (snapshot.connectionState == ConnectionState.waiting) {
                      //       return Center(
                      //         child: Text("Loading..."),
                      //       );
                      //     } else {
                      //       return ListView.builder(itemCount: snapshot.data.length, itemBuilder: (context, index) {
                      //         return _orderWidget(snapshot.data[index]);
                      //       },);
                      //     }
                      //   },
                      // );
                    });
                // return FutureBuilder<List<Order>>(
                //   future: userData.myShopId == null ? getUserOrders() : getShopOrders(userData.myShopId),
                //   builder: (context, AsyncSnapshot<List<Order>> snapshot) {
                //     if (snapshot.connectionState == ConnectionState.waiting) {
                //       return Center(
                //         child: Text("Loading..."),
                //       );
                //     } else {
                //       return ListView.builder(itemCount: snapshot.data.length, itemBuilder: (context, index) {
                //         return _orderWidget(snapshot.data[index]);
                //       },);
                //     }
                //   },
                // );
              }),
        ),
      ),
    );
  }

  _orderWidget(Order order) {
    final borderRadius = BorderRadius.all(Radius.circular(16.0));
    return GestureDetector(
      onTap: () => Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => PastOrderScreen(
                shopId: order.shopId,
                orderId: order.id,
              ))),
      child: Card(
        margin: const EdgeInsets.all(8.0),
//              margin: const EdgeInsets.symmetric(vertical: 16.0),
        elevation: 5,
//            color: backgroundColor,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Container(
            width: double.infinity,
            height: 80.0,
//          padding: const EdgeInsets.all(16.0),
            decoration: BoxDecoration(
              color: Colors.white.withAlpha(210),
              borderRadius: borderRadius,
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      order.orderStatus.stringValue(context),
                      style: TextStyle(
                        color: order.orderStatus.colorValue(context),
                        fontSize: 12.0,
                      ),
                    ),
                    SizedBox(
                      width: 8.0,
                    ),
                    Text(
                      order.dateCreatedStringValue(),
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 12.0,
                      ),
                    ),
                    Expanded(child: SizedBox()),
                    Text(
                      order.orderNumberStringValue(),
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 12.0,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 16.0),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Text(
                          order.cartProducts
                              .map((e) => e.title)
                              .reduce((a, b) => a + '; ' + b),
                          overflow: TextOverflow.fade,
                          maxLines: 5,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16.0,
                          ),
                        ),
                      ),
                      SizedBox(width: 16.0),
                      Text(
                        order.orderPriceStringValue(),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 22.0,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        shape: RoundedRectangleBorder(
          side: BorderSide(
            width: 4.0,
            color: order.orderStatus.colorValue(context),
          ),
          borderRadius: borderRadius,
        ),
      ),
    );
  }

  Future<List<Order>> getShopOrders(String shopId) async {
    var querySnap = await FirebaseFirestore.instance
        .collection(SHOPS_LINK)
        .doc(shopId)
        .collection('orders')
        .orderBy('dateCreated', descending: true)
        .get();
    return await _getShopOrders(querySnap.docs);
  }

  Future<List<Order>> getUserOrders() async {
    final querySnap = await FirebaseFirestore.instance
        .collection(USERS_LINK)
        .doc(FirebaseAuth.instance.currentUser.uid)
        .get();
    var ordersJson = querySnap.data()['orders'] ?? [];
    List<Order> orders = await _getOrders(ordersJson);
    orders.sort((a, b) => b.dateCreated.compareTo(a.dateCreated));
    return orders;
  }

  Future<List<Order>> _getShopOrders(List<DocumentSnapshot> ordersJson) async {
    List<Order> orders = [];
    for (DocumentSnapshot orderSnapshot in ordersJson) {
      Order order = Order.fromJson(orderSnapshot.data());
      final productsSnapshot =
          await orderSnapshot.reference.collection('products').get();
      List<CartProduct> cartProducts = [];
      for (var cartProductSnapshot in productsSnapshot.docs) {
        cartProducts.add(CartProduct.fromJson(cartProductSnapshot.data()));
      }
      order.cartProducts = cartProducts;
      orders.add(order);
    }
    return orders;
  }

  Future<List<Order>> _getOrders(dynamic ordersJson) async {
    List<Order> orders = [];
    for (DocumentReference orderReference in ordersJson) {
      final orderSnapshot = await orderReference.get();
      Order order = Order.fromJson(orderSnapshot.data());
      final productsSnapshot =
          await orderSnapshot.reference.collection('products').get();
      List<CartProduct> cartProducts = [];
      for (var cartProductSnapshot in productsSnapshot.docs) {
        cartProducts.add(CartProduct.fromJson(cartProductSnapshot.data()));
      }
      order.cartProducts = cartProducts;
      orders.add(order);
    }
    return orders;
  }

  Stream<DocumentSnapshot> getDocs(String uid) async* {
    var firestore = FirebaseFirestore.instance;

    var querySnap = await firestore
        .collection(USERS_LINK)
        .doc('W8aYzQhqKvgUqpbikdHomOnjnZJ2')
        .get();
    var ordersJson = querySnap.data()['orders'] ?? [];
    List<DocumentSnapshot> ordersSnapshots = [];
    for (DocumentReference orderReference in ordersJson) {
      yield await orderReference.get();
    }
  }

  Stream<int> timedCounter(Duration interval, [int maxCount]) async* {
    int i = 0;
    while (true) {
      await Future.delayed(interval);
      yield i++;
      if (i == maxCount) break;
    }
  }
}
