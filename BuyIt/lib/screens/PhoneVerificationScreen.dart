import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:BuyIt/model/AppState.dart';
import 'package:BuyIt/redux/actions.dart';
import 'package:BuyIt/extensions.dart';
import 'package:BuyIt/AppLocalizations.dart';

import '../Common.dart';
import 'MainScreen.dart';

class PhoneVerificationScreen extends StatefulWidget {
  final String phoneNumber;
  final String verificationId;
  final int resendToken;
  final bool moveToMainScreen;

  const PhoneVerificationScreen(
      {Key key,
      this.phoneNumber,
      this.verificationId,
      this.resendToken,
      this.moveToMainScreen = true})
      : super(key: key);

  @override
  _PhoneVerificationScreenState createState() =>
      _PhoneVerificationScreenState();
}

class _PhoneVerificationScreenState extends State<PhoneVerificationScreen> {
  var onTapRecognizer;
  StreamController<ErrorAnimationType> errorController;
  TextEditingController textEditingController = TextEditingController();

  bool hasError = false;
  String smsCode = "";
  final formKey = GlobalKey<FormState>();

  @override
  void initState() {
    onTapRecognizer = TapGestureRecognizer()
      ..onTap = () {
        Navigator.pop(context);
      };
    errorController = StreamController<ErrorAnimationType>();
    super.initState();
  }

  @override
  void dispose() {
    errorController.close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ).withBottomAdmobBanner(context),
      body: SafeArea(
        child: Center(
          child: Container(
//              width: 200.0,
            child: Column(
              children: [
//                  PinCodeTextField(
//                    length: 6,
//                    obsecureText: false,
//                    animationType: AnimationType.fade,
//                    pinTheme: PinTheme(
//                      shape: PinCodeFieldShape.box,
//                      borderRadius: BorderRadius.circular(5),
//                      fieldHeight: 50,
//                      fieldWidth: 40,
//                      activeFillColor: Colors.white,
//                    ),
//                    animationDuration: Duration(milliseconds: 300),
//                    backgroundColor: Colors.blue.shade50,
//                    enableActiveFill: true,
//                    errorAnimationController: errorController,
//                    controller: textEditingController,
//                    onCompleted: (v) {
//                      print("Completed");
//                    },
//                    onChanged: (value) {
//                      print(value);
//                      setState(() {
//                        smsCode = value;
//                      });
//                    },
//                    beforeTextPaste: (text) {
//                      print("Allowing to paste $text");
//                      //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
//                      //but you can show anything you want here, like your pop up saying wrong paste format or etc
//                      return true;
//                    },
//                  ),
//                  SizedBox(height: 16.0,),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Text(
                    AppLocalizations.of(context)
                        .translate('phone_number_verification'),
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 30.0, vertical: 8),
                  child: RichText(
                    text: TextSpan(
                        text: AppLocalizations.of(context)
                            .translate('enter_code_sent_to'),
                        children: [
                          TextSpan(
                              text: widget.phoneNumber,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15)),
                        ],
                        style: TextStyle(color: Colors.black54, fontSize: 15)),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Form(
                  key: formKey,
                  child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 30),
                      child: PinCodeTextField(
                        autoFocus: true,
                        keyboardType: TextInputType.number,
                        length: 6,
                        animationType: AnimationType.fade,
                        validator: (v) {
                          if (v.length < 3) {
                            return "I'm from validator";
                          } else {
                            return null;
                          }
                        },
                        pinTheme: PinTheme(
                          shape: PinCodeFieldShape.box,
                          borderRadius: BorderRadius.circular(5),
                          fieldHeight: 50,
                          fieldWidth: 40,
                          activeFillColor:
                              hasError ? Colors.orange : Colors.white,
                        ),
                        animationDuration: Duration(milliseconds: 300),
//                          backgroundColor: Colors.blue.shade50,
//                          enableActiveFill: true,
                        errorAnimationController: errorController,
                        controller: textEditingController,
                        onCompleted: (v) {
                          print("Completed");
                        },
                        onChanged: (value) {
                          print(value);
                          setState(() {
                            smsCode = value;
                          });
                        },
                        beforeTextPaste: (text) {
                          print("Allowing to paste $text");
                          //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                          //but you can show anything you want here, like your pop up saying wrong paste format or etc
                          return true;
                        },
                        appContext: context,
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Text(
                    hasError
                        ? "*${AppLocalizations.of(context).translate('please_fill_up_all_cells')}"
                        : "",
                    style: TextStyle(
                        color: Colors.red,
                        fontSize: 12,
                        fontWeight: FontWeight.w400),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                      text:
                          "${AppLocalizations.of(context).translate('didnt_receive_code')} ",
                      style: TextStyle(color: Colors.black54, fontSize: 15),
                      children: [
                        TextSpan(
                            text:
                                " ${AppLocalizations.of(context).translate('resend').toUpperCase()}",
                            recognizer: onTapRecognizer,
                            style: TextStyle(
                                color: Color(0xFF91D3B3),
                                fontWeight: FontWeight.bold,
                                fontSize: 16))
                      ]),
                ),
                SizedBox(
                  height: 14,
                ),
                Container(
                  margin: const EdgeInsets.symmetric(
                      vertical: 16.0, horizontal: 30),
                  child: ButtonTheme(
                    height: 50,
                    child: FlatButton(
                      onPressed: () {
                        formKey.currentState.validate();
                        if (smsCode.length != 6) {
                          _showError();
                        } else {
                          setState(() {
                            hasError = false;
                          });
                          _phoneSignIn();
                        }
//                        formKey.currentState.validate();
//                        // conditions for validating
//                        if (smsCode.length != 6 || smsCode != "towtow") {
//                          errorController.add(ErrorAnimationType
//                              .shake); // Triggering error shake animation
//                          setState(() {
//                            hasError = true;
//                          });
//                        } else {
//                          setState(() {
//                            hasError = false;
//                            scaffoldKey.currentState.showSnackBar(SnackBar(
//                              content: Text("Aye!!"),
//                              duration: Duration(seconds: 2),
//                            ));
//                          });
//                        }
                      },
                      child: Center(
                          child: Text(
                            AppLocalizations.of(context).translate('verify').toUpperCase(),
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      )),
                    ),
                  ),
                  decoration: BoxDecoration(
                      color: Colors.green.shade300,
                      borderRadius: BorderRadius.circular(5),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.green.shade200,
                            offset: Offset(1, -2),
                            blurRadius: 5),
                        BoxShadow(
                            color: Colors.green.shade200,
                            offset: Offset(-1, 2),
                            blurRadius: 5)
                      ]),
                ),
//                Container(
//                  width: double.infinity,
//                  height: 60.0,
//                  decoration: BoxDecoration(
//                    color: Theme.of(context).primaryColor,
//                    borderRadius: BorderRadius.all(
//                      Radius.circular(8.0),
//                    ),
//                  ),
//                  child: FlatButton(
//                      onPressed: () {
//                        Navigator.of(context).pop();
//                        Navigator.of(context).pop({'verified': true});
//                      },
//                      child: Text(
//                        'Готово',
//                        style: TextStyle(color: Colors.white, fontSize: 18.0),
//                      )),
//                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _showError() {
    errorController.add(ErrorAnimationType.shake);
    setState(() {
      hasError = true;
    });
  }

  _phoneSignIn() {
//    PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.credential(verificationId: widget.verificationId, smsCode: smsCode);
//    final ggg = await FirebaseAuth.instance.signInWithCredential(phoneAuthCredential);
    final overlayEntry = showProgressIndicator(context);
    StoreProvider.of<AppState>(context).dispatch(
      PhoneLogInAction(
        context,
        widget.verificationId,
        smsCode,
        () {
          overlayEntry.remove();
          if (widget.moveToMainScreen) {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                settings: RouteSettings(name: '/'),
                builder: (_) => MainScreen(),
//                  firstScreen == FirstScreen.intro ? Intro52() : MainScreen(),
              ),
            );
          } else {
            Navigator.of(context).pop();
            Navigator.of(context).pop({'verified': true});
          }
        },
        () {
          overlayEntry.remove();
          _showError();
        },
      ),
    );
  }
}
