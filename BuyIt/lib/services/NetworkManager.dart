import 'dart:convert';

import 'package:BuyIt/model/AppState.dart';
import 'package:BuyIt/redux/actions.dart';
import 'package:BuyIt/screens/PastOrderScreen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../Common.dart';

class NetworkManager {
  NetworkManager._();

  factory NetworkManager() => _instance;

  static final NetworkManager _instance = NetworkManager._();

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  bool _initialized = false;

  Future<void> init1(BuildContext context) async {
    if (!_initialized) {
      // For iOS request permission first.
      _firebaseMessaging.requestNotificationPermissions();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');
        final order1 = Map.castFrom(json.decode(message['data']['action']));
        try {
          Map<String, dynamic> actionJson = jsonDecode(message['data']['action']);

          final order = Map<String, String>.from(json.decode(message['data']['action']));
          final order22 = order['open_order'];
          _openOrder(context, order);
          // final order223 = order['open_order'];
        } catch (e) {
          print(e);
        }
        // final order = Map<String, dynamic>.from(json.decode(message['data']['action']))['open_order'];
        // _openOrder(context, order);
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
        final order = Map.castFrom(json.decode(message['data']['action']))['open_order'];
        _openOrder(context, order);
        // add(NavigateToView(path));
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
        final order1 = Map.castFrom(json.decode(message['data']['action']));
        final order = Map.castFrom(json.decode(message['data']['action']))['open_order'];
        _openOrder(context, order);
        // add(NavigateToView(path));
      },
    );

      // For testing purposes print the Firebase Messaging token
      String token = await _firebaseMessaging.getToken();
      print("FirebaseMessaging token: $token");

      _initialized = true;
    }
  }

  _openOrder(BuildContext context, Map<String, String> order) {
    final shopId = order.keys.first;
    StoreProvider.of<AppState>(context).dispatch(LoadOrdersAction(context: context, myShopId: StoreProvider.of<AppState>(context).state.userData.myShopId, response: (data, error) {
      if (error == null) {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => PastOrderScreen(
              shopId: shopId,
              orderId: order[shopId],
            )));
      }
    }));
  }

  static saveUserToken(String token, String uid) async {
    List<dynamic> tokens = await getUserTokens(uid);
    if (!tokens.contains(token)) {
      FirebaseFirestore.instance
          .collection(USERS_LINK)
          .doc(uid).set({'tokens': FieldValue.arrayUnion([token])}, SetOptions(merge: true));
    }
  }

  static Future<List<dynamic>> getUserTokens(String uid) async {
    final querySnap = await FirebaseFirestore.instance
        .collection(USERS_LINK)
        .doc(uid)
        .get();
    return querySnap.data() == null ? [] : querySnap.data()['tokens'] ?? [];
  }
}
