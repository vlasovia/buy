

import 'package:shared_preferences/shared_preferences.dart';

class LocalStoreService {
  static const CART_ID = 'cart';

  static saveCart(List<String> cart) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setStringList(CART_ID, cart);
  }

  static clearCart() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove(CART_ID);
  }

  static Future<List<String>> restoreCart() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList(CART_ID);
  }
}