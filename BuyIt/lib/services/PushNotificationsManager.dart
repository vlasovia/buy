import 'dart:convert';

import 'package:BuyIt/Common.dart';
import 'package:BuyIt/main.dart';
import 'package:BuyIt/model/AppState.dart';
import 'package:BuyIt/redux/actions.dart';
import 'package:BuyIt/screens/PastOrderScreen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:BuyIt/AppLocalizations.dart';

class PushNotificationsManager {
  PushNotificationsManager._();

  factory PushNotificationsManager() => _instance;

  static final PushNotificationsManager _instance =
      PushNotificationsManager._();

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  bool _initialized = false;

  Future<String> init(BuildContext context) async {
    if (!_initialized) {
      // For iOS request permission first.
      _firebaseMessaging.requestNotificationPermissions();
      _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          print('on message $message');
          // final order1 = Map.castFrom(json.decode(message['data']['action']));
          try {
            final notificationJson = message['notification'];
            final dataJson = message['data'];
            final shopId = dataJson['shopId'];
            final orderId = dataJson['orderId'];
            showSnackBar(
                notificationJson["title"],
                notificationJson["body"],
                AppLocalizations.of(context).translate('look_at'),
                () => _openOrder(context, shopId, orderId));
          } catch (e) {
            print(e);
          }
          // final order = Map<String, dynamic>.from(json.decode(message['data']['action']))['open_order'];
          // _openOrder(context, order);
        },
        onResume: (Map<String, dynamic> message) async {
          print('on resume $message');
          try {
            final dataJson = message['data'];
            final shopId = dataJson['shopId'];
            final orderId = dataJson['orderId'];
            _openOrder(context, shopId, orderId);
          } catch (e) {
            print(e);
          }
        },
        onLaunch: (Map<String, dynamic> message) async {
          print('on launch $message');
          try {
            final dataJson = message['data'];
            final shopId = dataJson['shopId'];
            final orderId = dataJson['orderId'];
            await Future.delayed(Duration(seconds: 1));
            _openOrder(context, shopId, orderId);
          } catch (e) {
            print(e);
          }
        },
      );

      // For testing purposes print the Firebase Messaging token
      String token = await _firebaseMessaging.getToken();
      print("FirebaseMessaging token: $token");

      _initialized = true;
      return token;
    }
    String token = await _firebaseMessaging.getToken();
    print("FirebaseMessaging token: $token");

    _initialized = true;
    return token;
  }

  _openOrder(BuildContext context, String shopId, String orderId) {
    StoreProvider.of<AppState>(MyApp.navigatorKey.currentContext).dispatch(
        LoadOrdersAction(
            context: MyApp.navigatorKey.currentContext,
            myShopId:
                StoreProvider.of<AppState>(MyApp.navigatorKey.currentContext)
                    .state
                    .userData
                    .myShopId,
            response: (data, error) {
              if (error == null) {
                Navigator.of(MyApp.navigatorKey.currentContext)
                    .push(MaterialPageRoute(
                        builder: (context) => PastOrderScreen(
                              shopId: shopId,
                              orderId: orderId,
                            )));
              }
            }));
  }
// _openOrder(BuildContext context, Map<String, String> order) {
//   final shopId = order.keys.first;
//   StoreProvider.of<AppState>(context).dispatch(LoadOrdersAction(context: context, myShopId: StoreProvider.of<AppState>(context).state.userData.myShopId, response: (data, error) {
//     if (error == null) {
//       Navigator.of(context).push(MaterialPageRoute(
//           builder: (context) => PastOrderScreen(
//             shopId: shopId,
//             orderId: order[shopId],
//           )));
//     }
//   }));
// }
}
