import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

//import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:firebase_core/firebase_core.dart';

import 'AppLocalizations.dart';
import 'model/AppState.dart';
import 'redux/middleware.dart';
import 'redux/reducers.dart';
import 'screens/LoginScreen.dart';
import 'screens/MainScreen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Admob.initialize(testDeviceIds: ['A4E047867CCE48DB341BFB9D5A2F6228']);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final store = new Store<AppState>(appStateReducers,
      initialState: AppState.empty, middleware: [createStoreMiddleware]);
  static final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();

  final _color = {
    50: Color.fromRGBO(255, 139, 2, .1),
    100: Color.fromRGBO(255, 139, 2, .2),
    200: Color.fromRGBO(255, 139, 2, .3),
    300: Color.fromRGBO(255, 139, 2, .4),
    400: Color.fromRGBO(255, 139, 2, .5),
    500: Color.fromRGBO(255, 139, 2, .6),
    600: Color.fromRGBO(255, 139, 2, .7),
    700: Color.fromRGBO(255, 139, 2, .8),
    800: Color.fromRGBO(255, 139, 2, .9),
    900: Color.fromRGBO(255, 139, 2, 1),
  };

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        navigatorKey: navigatorKey,
        localizationsDelegates: [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate
        ],
        supportedLocales: [
          const Locale('en'),
          const Locale('ua'),
          const Locale('uk'),
          const Locale('ru'),
        ],
        title: 'Sell',
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: MaterialColor(0xFFFF8B02, _color),
          // This makes the visual density adapt to the platform that you run
          // the app on. For desktop platforms, the controls will be smaller and
          // closer together (more dense) than on mobile platforms.
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
//        home: MainScreen(),
        home: FutureBuilder(
          // Initialize FlutterFire
          future: Firebase.initializeApp(),
          builder: (context, snapshot) {
            // Check for errors
            if (snapshot.hasError) {
              return Container();
            }

            // Once complete, show your application
            if (snapshot.connectionState == ConnectionState.done) {
              return MainScreen();
            }

            // Otherwise, show something whilst waiting for initialization to complete
            return Container();
          },
        ),
      ),
    );
  }
}

class MainScreen1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          AppLocalizations.of(context).translate('shops'),
        ),
        leading: Builder(builder: (BuildContext context) {
          return IconButton(
            icon: Icon(Icons.person),
            color: Colors.black38,
            onPressed: () {
              Scaffold.of(context).openDrawer();
            },
          );
        }),
      ),
      body: SafeArea(
        child: Column(
          children: [
            TextField(
              maxLines: 1,
              decoration: InputDecoration(
                  hintText: AppLocalizations.of(context).translate('name')),
            ),
            TextField(
              maxLines: 4,
              decoration: InputDecoration(
                  hintText:
                      AppLocalizations.of(context).translate('description')),
            ),
          ],
        ),
      ),
    );
  }
}

//
//   @override
//   Widget build(BuildContext context) {
//     return StoreProvider<AppState>(
//       store: store,
//       child: MaterialApp(
//         debugShowCheckedModeBanner: false,
//         navigatorKey: navigatorKey,
//        localizationsDelegates: [
//          AppLocalizations.delegate,
//          GlobalMaterialLocalizations.delegate,
//          GlobalWidgetsLocalizations.delegate,
//          GlobalCupertinoLocalizations.delegate,
//        ],
//        supportedLocales: [
//          const Locale('ua'),
//          const Locale('ru'),
//        ],
//         title: 'Sell',
//         theme: ThemeData(
//           // This is the theme of your application.
//           //
//           // Try running your application with "flutter run". You'll see the
//           // application has a blue toolbar. Then, without quitting the app, try
//           // changing the primarySwatch below to Colors.green and then invoke
//           // "hot reload" (press "r" in the console where you ran "flutter run",
//           // or simply save your changes to "hot reload" in a Flutter IDE).
//           // Notice that the counter didn't reset back to zero; the application
//           // is not restarted.
//           primarySwatch: MaterialColor(0xFFFF8B02, _color),
//           // This makes the visual density adapt to the platform that you run
//           // the app on. For desktop platforms, the controls will be smaller and
//           // closer together (more dense) than on mobile platforms.
//           visualDensity: VisualDensity.adaptivePlatformDensity,
//         ),
// //        home: MainScreen(),
//         home: FutureBuilder(
//           // Initialize FlutterFire
//           future: Firebase.initializeApp(),
//           builder: (context, snapshot) {
//             // Check for errors
//             if (snapshot.hasError) {
//               return Container();
//             }
//
//             // Once complete, show your application
//             if (snapshot.connectionState == ConnectionState.done) {
//               return MainScreen();
//             }
//
//             // Otherwise, show something whilst waiting for initialization to complete
//             return Container();
//           },
//         ),
//       ),
//     );
//   }
// }

//class MyHomePage extends StatefulWidget {
//  MyHomePage({Key key, this.title}) : super(key: key);
//
//  // This widget is the home page of your application. It is stateful, meaning
//  // that it has a State object (defined below) that contains fields that affect
//  // how it looks.
//
//  // This class is the configuration for the state. It holds the values (in this
//  // case the title) provided by the parent (in this case the App widget) and
//  // used by the build method of the State. Fields in a Widget subclass are
//  // always marked "final".
//
//  final String title;
//
//  @override
//  _MyHomePageState createState() => _MyHomePageState();
//}
//
//class _MyHomePageState extends State<MyHomePage> {
//  int _counter = 0;
//
//  void _incrementCounter() {
//    setState(() {
//      // This call to setState tells the Flutter framework that something has
//      // changed in this State, which causes it to rerun the build method below
//      // so that the display can reflect the updated values. If we changed
//      // _counter without calling setState(), then the build method would not be
//      // called again, and so nothing would appear to happen.
//      _counter++;
//    });
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    // This method is rerun every time setState is called, for instance as done
//    // by the _incrementCounter method above.
//    //
//    // The Flutter framework has been optimized to make rerunning build methods
//    // fast, so that you can just rebuild anything that needs updating rather
//    // than having to individually change instances of widgets.
//    return Scaffold(
//      appBar: AppBar(
//        // Here we take the value from the MyHomePage object that was created by
//        // the App.build method, and use it to set our appbar title.
//        title: Text(AppLocalizations.of(context).translate('title')),
//      ),
//      body: Center(
//        // Center is a layout widget. It takes a single child and positions it
//        // in the middle of the parent.
//        child: Column(
//          // Column is also a layout widget. It takes a list of children and
//          // arranges them vertically. By default, it sizes itself to fit its
//          // children horizontally, and tries to be as tall as its parent.
//          //
//          // Invoke "debug painting" (press "p" in the console, choose the
//          // "Toggle Debug Paint" action from the Flutter Inspector in Android
//          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
//          // to see the wireframe for each widget.
//          //
//          // Column has various properties to control how it sizes itself and
//          // how it positions its children. Here we use mainAxisAlignment to
//          // center the children vertically; the main axis here is the vertical
//          // axis because Columns are vertical (the cross axis would be
//          // horizontal).
//          mainAxisAlignment: MainAxisAlignment.center,
//          children: <Widget>[
//            Text(
//              AppLocalizations.of(context).translate('Message'),
//            ),
//            Text(
//              '$_counter',
//              style: Theme.of(context).textTheme.headline4,
//            ),
//          ],
//        ),
//      ),
//      floatingActionButton: FloatingActionButton(
//        onPressed: _incrementCounter,
//        tooltip: 'Increment',
//        child: Icon(Icons.add),
//      ), // This trailing comma makes auto-formatting nicer for build methods.
//    );
//  }
//}
