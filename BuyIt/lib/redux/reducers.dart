import 'dart:convert';
import 'package:redux/redux.dart';

import 'package:BuyIt/model/AppState.dart';
import 'package:BuyIt/model/CartProduct.dart';
import 'package:BuyIt/model/Shop.dart';
import 'package:BuyIt/services/LocalStoreService.dart';

import 'actions.dart';

final appStateReducers = combineReducers<AppState>([
  TypedReducer<AppState, UserCheckedAction>(_userCheckedReducer),
  TypedReducer<AppState, RestoreCartProductsCompletedAction>(
      _restoreCartProductsCompletedReducer),
  TypedReducer<AppState, UserLoggedInAction>(_userLoggedInReducer),
  TypedReducer<AppState, UserLogOutAction>(_userLogOutReducer),
  TypedReducer<AppState, ShopsLoadedAction>(_shopsLoadedReducer),
  TypedReducer<AppState, ProductTypesUpdatedAction>(
      _productTypesUpdatedReducer),
  TypedReducer<AppState, ShopProductsLoadedAction>(_shopProductsLoadedReducer),
  TypedReducer<AppState, AddProductToCartAction>(_addProductToCartReducer),
  TypedReducer<AppState, RemoveProductFromCartAction>(
      _removeProductFromCartReducer),
  TypedReducer<AppState, ChangeCartProductQuantityAction>(
      _changeCartProductQuantityReducer),
  TypedReducer<AppState, GetUserDataAction>(_getUserDataReducer),
  TypedReducer<AppState, UpdateUserDataAction>(_updateUserDataReducer),
  TypedReducer<AppState, OrdersLoadedAction>(_ordersLoadedReducer),
  TypedReducer<AppState, OrderUpdatedAction>(_orderUpdatedReducer),
  TypedReducer<AppState, ClearCartAction>(_clearCartReducer),
]);

AppState _userCheckedReducer(AppState state, UserCheckedAction action) {
  return AppState(
    user: action.user,
    userData: state.userData,
    shops: [],
    productTypes: state.productTypes,
    cartProducts: state.cartProducts,
  );
}

AppState _restoreCartProductsCompletedReducer(
    AppState state, RestoreCartProductsCompletedAction action) {
  return AppState(
    user: state.user,
    userData: state.userData,
    shops: state.shops,
    productTypes: state.productTypes,
    cartProducts: action.cartProducts,
  );
}

AppState _userLoggedInReducer(AppState state, UserLoggedInAction action) {
  return AppState(
    user: action.user,
    userData: state.userData,
    shops: state.shops ?? [],
    productTypes: state.productTypes,
    cartProducts: state.cartProducts,
  );
}

AppState _userLogOutReducer(AppState state, UserLogOutAction action) {
  return AppState.empty;
}

AppState _shopsLoadedReducer(AppState state, ShopsLoadedAction action) {
  if (state.shops != null && state.shops.length > 0) {
    action.shops.forEach((newShop) {
      print(newShop.titleImage);
      final shopIndex = state.shops.indexWhere((shop) => shop.id == newShop.id);
      if (shopIndex != -1) {
        newShop.products = state.shops[shopIndex].products;
      }
    });
  }
  return AppState(
    user: state.user,
    userData: state.userData,
    shops: action.shops,
    productTypes: state.productTypes,
    cartProducts: state.cartProducts,
  );
}

AppState _productTypesUpdatedReducer(
    AppState state, ProductTypesUpdatedAction action) {
  return AppState(
    user: state.user,
    userData: state.userData,
    shops: state.shops,
    productTypes: action.productTypes,
    cartProducts: state.cartProducts,
  );
}

AppState _shopProductsLoadedReducer(
    AppState state, ShopProductsLoadedAction action) {
  List<Shop> newShops = state.shops;
  final index = newShops.indexWhere((shop) => shop.id == action.shopId);
  newShops[index].products = action.products;
  return AppState(
    user: state.user,
    userData: state.userData,
    shops: newShops,
    productTypes: state.productTypes,
    cartProducts: state.cartProducts,
  );
}

AppState _addProductToCartReducer(
    AppState state, AddProductToCartAction action) {
  List<CartProduct> newCartProducts =
      state.cartProducts == null ? [] : state.cartProducts;
  newCartProducts.add(CartProduct(action.shopId, action.productId, action.title,
      action.description, action.price, action.quantity));
  LocalStoreService.saveCart(
      newCartProducts.map((CartProduct e) => jsonEncode(e.toJson())).toList());
  return AppState(
    user: state.user,
    userData: state.userData,
    shops: state.shops,
    productTypes: state.productTypes,
    cartProducts: newCartProducts,
  );
}

AppState _removeProductFromCartReducer(
    AppState state, RemoveProductFromCartAction action) {
  List<CartProduct> newCartProducts =
      state.cartProducts == null ? [] : state.cartProducts;
  newCartProducts.removeWhere((cartProduct) =>
      cartProduct.productId == action.productId &&
      cartProduct.shopId == action.shopId);
  LocalStoreService.saveCart(
      newCartProducts.map((CartProduct e) => jsonEncode(e.toJson())).toList());
  return AppState(
    user: state.user,
    userData: state.userData,
    shops: state.shops,
    productTypes: state.productTypes,
    cartProducts: newCartProducts,
  );
}

AppState _changeCartProductQuantityReducer(
    AppState state, ChangeCartProductQuantityAction action) {
  List<CartProduct> newCartProducts = state.cartProducts;
  int index = newCartProducts
      .indexWhere((cartProduct) => cartProduct.productId == action.productId);
  CartProduct cartProduct = newCartProducts[index];
  CartProduct newCartProduct = cartProduct.withQuantity(action.quantity);
  newCartProducts.replaceRange(index, index + 1, [newCartProduct]);
  LocalStoreService.saveCart(
      newCartProducts.map((CartProduct e) => jsonEncode(e.toJson())).toList());
  return AppState(
    user: state.user,
    userData: state.userData,
    shops: state.shops,
    productTypes: state.productTypes,
    cartProducts: newCartProducts,
  );
}

AppState _getUserDataReducer(AppState state, GetUserDataAction action) {
  return AppState(
    user: state.user,
    userData: action.userData,
    shops: state.shops,
    productTypes: state.productTypes,
    cartProducts: state.cartProducts,
  );
}

AppState _updateUserDataReducer(AppState state, UpdateUserDataAction action) {
  return AppState(
    user: state.user,
    userData: action.userData,
    shops: state.shops,
    productTypes: state.productTypes,
    cartProducts: state.cartProducts,
  );
}

AppState _ordersLoadedReducer(AppState state, OrdersLoadedAction action) {
  return AppState(
    user: state.user,
    userData: state.userData,
    shops: state.shops,
    productTypes: state.productTypes,
    cartProducts: state.cartProducts,
    orders: action.orders,
  );
}

AppState _orderUpdatedReducer(AppState state, OrderUpdatedAction action) {
  final orders = action.order == null
      ? state.orders
      : state.orders
          .map((order) => order.id == action.order.id ? action.order : order)
          .toList();
  return AppState(
    user: state.user,
    userData: state.userData,
    shops: state.shops,
    productTypes: state.productTypes,
    cartProducts: state.cartProducts,
    orders: orders,
  );
}

AppState _clearCartReducer(AppState state, ClearCartAction action) {
  LocalStoreService.clearCart();
  return AppState(
    user: state.user,
    userData: state.userData,
    shops: state.shops,
    productTypes: state.productTypes,
    cartProducts: null,
    orders: state.orders,
  );
}
