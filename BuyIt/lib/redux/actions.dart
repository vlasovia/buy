import 'package:BuyIt/model/UserData.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:BuyIt/model/CartProduct.dart';
import 'package:BuyIt/model/Order.dart';
import 'package:BuyIt/model/Product.dart';
import 'package:BuyIt/model/ProductType.dart';
import 'package:BuyIt/model/Shop.dart';

class UserLogOutAction {}

abstract class UserLogInAction {
  final BuildContext context;
  final VoidCallback loggedIn;
  final VoidCallback apiErrorCallback;

  UserLogInAction(
    this.context,
    this.loggedIn,
    this.apiErrorCallback,
  );
}

class GoogleLogInAction extends UserLogInAction {
  final String email;
  final String password;

  GoogleLogInAction(BuildContext context, this.email, this.password, loggedIn,
      apiErrorCallback)
      : super(context, loggedIn, apiErrorCallback);
}

class PhoneLogInAction extends UserLogInAction {
  final String verificationId;
  final String smsCode;

  PhoneLogInAction(BuildContext context, this.verificationId, this.smsCode,
      loggedIn, apiErrorCallback)
      : super(context, loggedIn, apiErrorCallback);
}

class UserLoggedInAction {
  final User user;

  UserLoggedInAction(this.user);
}

class CheckUserAction {
  final BuildContext context;

  CheckUserAction(this.context);
}

class UserCheckedAction {
  final User user;

  UserCheckedAction(this.user);
}

class LoadShopsAction {
  final BuildContext context;

  LoadShopsAction(this.context);
}

class ShopsLoadedAction {
  final BuildContext context;
  final List<Shop> shops;

  ShopsLoadedAction(this.context, this.shops);
}

class LoadOrdersAction {
  final BuildContext context;
  final String myShopId;
  final Response response;

  LoadOrdersAction({this.context, this.myShopId, this.response});
}

class OrdersLoadedAction {
  final BuildContext context;
  final List<Order> orders;

  OrdersLoadedAction(this.context, this.orders);
}

class LoadShopProductsAction {
  final BuildContext context;
  final String shopId;

  LoadShopProductsAction(this.context, this.shopId);
}

class ShopProductsLoadedAction {
  final BuildContext context;
  final String shopId;
  final List<Product> products;

  ShopProductsLoadedAction(this.context, this.shopId, this.products);
}

class ProductTypesUpdatedAction {
  final List<ProductType> productTypes;

  ProductTypesUpdatedAction(this.productTypes);
}

class AddProductToCartAction {
  final String shopId;
  final String productId;
  final String title;
  final String description;
  final double price;
  final double quantity;

  AddProductToCartAction(this.shopId, this.productId, this.title, this.description, this.price, this.quantity);
}

class RemoveProductFromCartAction {
  final String shopId;
  final String productId;

  RemoveProductFromCartAction(this.shopId, this.productId);
}

class ChangeCartProductQuantityAction {
  final String productId;
  final double quantity;

  ChangeCartProductQuantityAction(this.productId, this.quantity);
}

class RestoreCartProductsAction {
  RestoreCartProductsAction();
}

class RestoreCartProductsCompletedAction {
  final List<CartProduct> cartProducts;

  RestoreCartProductsCompletedAction(this.cartProducts);
}

class AddOrderAction {
  final BuildContext context;
  final String firstName;
  final String lastName;
  final int delivery;
  final String address;
  final String comment;
  final Response response;

  AddOrderAction({this.context, this.firstName, this.lastName, this.delivery, this.address, this.comment, this.response});
}

class GetUserDataAction {
  final UserData userData;

  GetUserDataAction(this.userData);
}

class UpdateUserDataAction {
  final UserData userData;

  UpdateUserDataAction(this.userData);
}

class UpdatePastOrderAction {
  final BuildContext context;
  final Order order;
  final Response response;

  UpdatePastOrderAction({this.context, this.order, this.response});
}

class OrderUpdatedAction {
  final Order order;

  OrderUpdatedAction({this.order});
}

class ClearCartAction {
}

// class OpenOrderAction {
//   final String orderId;
//   final Response response;
//
//   OpenOrderAction({this.orderId});
// }

typedef Response(dynamic data, Error error);