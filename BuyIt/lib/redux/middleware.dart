import 'dart:convert';
import 'package:BuyIt/model/Order.dart';
import 'package:BuyIt/model/UserData.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';

import 'package:BuyIt/model/AppState.dart';
import 'package:BuyIt/model/CartProduct.dart';
import 'package:BuyIt/model/Product.dart';
import 'package:BuyIt/model/ProductType.dart';
import 'package:BuyIt/services/LocalStoreService.dart';
import 'package:BuyIt/services/NetworkManager.dart';
import 'package:BuyIt/services/PushNotificationsManager.dart';
import 'package:BuyIt/AppLocalizations.dart';

import '../Common.dart';
import 'actions.dart';

import 'package:BuyIt/model/Shop.dart';

void createStoreMiddleware(Store<AppState> store, action, NextDispatcher next) {
  if (action is CheckUserAction) {
    if (FirebaseAuth.instance.currentUser == null) {
      FirebaseAuth.instance.signInAnonymously().then(
        (authResult) {
          _completeUserCheck(store, action, authResult.user);
        },
      );
    } else {
      _completeUserCheck(store, action, FirebaseAuth.instance.currentUser);
    }
  } else if (action is RestoreCartProductsAction) {
    _restoreCartProducts(store, action);
  } else if (action is PhoneLogInAction) {
    _phoneSignIn(store, action);
  } else if (action is GoogleLogInAction) {
//      parameters: {'email': 'vlasovia@gmail.com', 'password': 'password'},
//        parameters: {'email': 'ilich.82@inbox.ru', 'password': 'password'},
//      parameters: {'email': 'ilich.82@mail.ru', 'password': 'p@ssword'},
    _googleSignIn(store, action);
//    _googleSignIn(action).then((user) {
//      if(user != null) {
//        store.dispatch(UserLoggedInAction(user));
//        action.loggedIn();
//      }
//    });
  } else if (action is LoadShopsAction) {
    _loadShops(store, action);
  } else if (action is LoadOrdersAction) {
    _loadOrders(store, action);
  } else if (action is LoadShopProductsAction) {
    _loadProducts(store, action);
  } else if (action is AddOrderAction) {
    _addOrder(store, action);
  } else if (action is UpdatePastOrderAction) {
    _updatePastOrder(store, action);
  } else if (action is UserLogOutAction) {
    FirebaseAuth.instance.signOut();
  }
  next(action);
}

_loadShops(Store<AppState> store, LoadShopsAction action) async {
  final querySnapshot = await FirebaseFirestore.instance
      .collection(SHOPS_LINK)
      .where('available', isEqualTo: true)
      .get();
  List<Shop> shops = [];
  for (var shopDocument in querySnapshot.docs) {
    var shopJson = shopDocument.data();
    shopJson['id'] = shopDocument.id;
    final imageReference = FirebaseStorage().ref().child('sandbox').child(
        'storesTitlesImages/${shopDocument.id}/${shopJson['titleImage']}');
    shopJson['titleImageUrl'] = await imageReference.getDownloadURL();
    Shop shop = Shop.fromJson(shopJson);
    print('Shop name: ${shop.name}');
    shops.add(shop);
//    shops.add(shop);
  }
  store.dispatch(ShopsLoadedAction(action.context, shops));
  PushNotificationsManager().init(action.context).then((token) {
    NetworkManager.saveUserToken(token, FirebaseAuth.instance.currentUser.uid);
  });
}

_loadOrders(Store<AppState> store, LoadOrdersAction action) async {
  try {
    final orders = action.myShopId == null
        ? await getUserOrders()
        : await getShopOrders(action.myShopId);
    store.dispatch(OrdersLoadedAction(action.context, orders));
    if (action.response != null) {
      action.response(null, null);
    }
  } catch (error) {
    if (action.response != null) {
      action.response(null, error);
    }
  }
}

Future<List<Order>> getShopOrders(String shopId) async {
  var querySnap = await FirebaseFirestore.instance
      .collection(SHOPS_LINK)
      .doc(shopId)
      .collection('orders')
      .orderBy('dateCreated', descending: true)
      .get();
  return await _getShopOrders(querySnap.docs);
}

Future<List<Order>> getUserOrders() async {
  final querySnap = await FirebaseFirestore.instance
      .collection(USERS_LINK)
      .doc(FirebaseAuth.instance.currentUser.uid)
      .get();
  var ordersJson = querySnap.data()['orders'] ?? [];
  List<Order> orders = await _getOrders(ordersJson);
  orders.sort((a, b) => b.dateCreated.compareTo(a.dateCreated));
  return orders;
}

Future<List<Order>> _getShopOrders(List<DocumentSnapshot> ordersJson) async {
  List<Order> orders = [];
  for (DocumentSnapshot orderSnapshot in ordersJson) {
    var orderJson = orderSnapshot.data();
    orderJson['id'] = orderSnapshot.id;
    Order order = Order.fromJson(orderJson);
    final productsSnapshot =
        await orderSnapshot.reference.collection('products').get();
    List<CartProduct> cartProducts = [];
    for (var cartProductSnapshot in productsSnapshot.docs) {
      cartProducts.add(CartProduct.fromJson(cartProductSnapshot.data()));
    }
    order.cartProducts = cartProducts;
    orders.add(order);
  }
  return orders;
}

Future<List<Order>> _getOrders(dynamic ordersJson) async {
  List<Order> orders = [];
  for (DocumentReference orderReference in ordersJson) {
    final orderSnapshot = await orderReference.get();
    var orderJson = orderSnapshot.data();
    orderJson['id'] = orderSnapshot.id;
    Order order = Order.fromJson(orderJson);
    final productsSnapshot =
        await orderSnapshot.reference.collection('products').get();
    List<CartProduct> cartProducts = [];
    for (var cartProductSnapshot in productsSnapshot.docs) {
      cartProducts.add(CartProduct.fromJson(cartProductSnapshot.data()));
    }
    order.cartProducts = cartProducts;
    orders.add(order);
  }
  return orders;
}

_restoreCartProducts(
    Store<AppState> store, RestoreCartProductsAction action) async {
  final jj = await LocalStoreService.restoreCart();
  List<CartProduct> cartProducts = (jj ?? []).map((e) {
    Map<String, dynamic> userJson = jsonDecode(e);
    final cartProduct = CartProduct.fromJson(userJson);
    return cartProduct;
  }).toList();
  store.dispatch(RestoreCartProductsCompletedAction(cartProducts));
}

_completeUserCheck(Store<AppState> store, CheckUserAction action, User user) {
  // PushNotificationsManager().init(action.context).then((token) {
  //   NetworkManager.saveUserToken(token, user.uid);
  // });
  store.dispatch(RestoreCartProductsAction());
  store.dispatch(UserCheckedAction(user));
  store.dispatch(LoadShopsAction(action.context));
  // store.dispatch(LoadOrdersAction(context: action.context, myShopId: user.m));
  FirebaseFirestore.instance
      .collection('product_types_test')
      .snapshots()
      .listen(
    (querySnapshot) {
      List<ProductType> productTypes = [];
      for (var productTypeDocument in querySnapshot.docs) {
        var productTypeJson = productTypeDocument.data();
        productTypeJson['id'] = productTypeDocument.id;
        ProductType productType = ProductType.fromJson(productTypeJson);
        productTypes.add(productType);
      }
      store.dispatch(ProductTypesUpdatedAction(productTypes));
    },
  );
  FirebaseFirestore.instance
      .collection(USERS_LINK)
      .doc(user.uid)
      .get()
      .then((userSnap) {
    var userJson = userSnap.data();
    store.dispatch(UpdateUserDataAction(
        userJson == null ? null : UserData.fromJson(userJson)));
//    var userData = UserData.fromJson(userJson);
//    store.dispatch(GetUserDataAction(userJson['shop']));
//    if (userSnap.data()['shop'] != null) {
//      store.dispatch(GetUserDataAction(userJson['shop']));
//    }
  });
}

_loadProducts(Store<AppState> store, LoadShopProductsAction action) async {
  var productsReference = FirebaseFirestore.instance
      .collection(SHOPS_LINK)
      .doc(action.shopId)
      .collection('products');
  QuerySnapshot querySnapshot;
  if ((store.state.userData?.myShopId ?? '') != action.shopId) {
    querySnapshot = await productsReference.where('status',
        whereIn: [ProductStatus.Active.index, ProductStatus.Out.index]).get();
  } else {
    querySnapshot = await productsReference.get();
  }
  List<Product> products = [];
  for (var productDocument in querySnapshot.docs) {
    var productJson = productDocument.data();
    productJson['id'] = productDocument.id;
    final imageReference = FirebaseStorage()
        .ref()
        .child('sandbox')
        .child('storesProductsImages/${action.shopId}/${productJson['image']}');
    productJson['imageUrl'] = await imageReference.getDownloadURL();
    Product product = Product.fromJson(productJson);
    products.add(product);
  }
  store.dispatch(
      ShopProductsLoadedAction(action.context, action.shopId, products));
}

_addOrder(Store<AppState> store, AddOrderAction action) async {
  final userData = UserData(
      firstName: action.firstName,
      lastName: action.lastName,
      address: action.address);
  FirebaseFirestore.instance.runTransaction((transaction) async {
    Map<String, List<Map<String, dynamic>>> orders = {};
    for (var cartProduct in store.state.cartProducts) {
      if (orders[cartProduct.shopId] == null) {
        orders[cartProduct.shopId] = [];
      }
      final cartProductJson = cartProduct.toJson();
      cartProductJson['productRef'] = FirebaseFirestore.instance.doc(
          '$SHOPS_LINK/${cartProduct.shopId}/products/${cartProduct.productId}');
      orders[cartProduct.shopId].add(cartProductJson);
    }
    final Map<String, String> newOrdersIds = {};
    for (var order in orders.entries) {
      final shop =
          FirebaseFirestore.instance.collection(SHOPS_LINK).doc(order.key);
      final newOrderDocument = shop.collection("orders").doc();
      final ordersNumberDocumentSnapshot = await transaction.get(shop);
      int ordersNumber = ordersNumberDocumentSnapshot.data() == null
          ? 0
          : ordersNumberDocumentSnapshot.data()['ordersNumber'];
      transaction.update(shop, {'ordersNumber': ++ordersNumber});

      final orderPrice = order.value.map((e) {
        final cartProduct = CartProduct.fromJson(e);
        return cartProduct.quantity * cartProduct.price;
      }).reduce((a, b) => a + b);
      final shopId = order.key;
      final orderJson = Order(
        orderNumber: ordersNumber,
        shopId: shopId,
        address: action.address,
        comment: action.comment,
        orderStatus: OrderStatus.New,
        uid: store.state.user.uid,
        phone: store.state.user.phoneNumber,
        deliveryType: action.delivery,
        dateCreated: DateTime.now(),
        price: orderPrice,
      ).toJson();
      transaction.set(newOrderDocument, orderJson);
      for (var cartProduct in order.value) {
        transaction.set(
            newOrderDocument.collection('products').doc(), cartProduct);
      }
      var userDataJson = userData.toJson();
      userDataJson['orders'] = FieldValue.arrayUnion([newOrderDocument]);
      transaction.set(
        FirebaseFirestore.instance
            .collection(USERS_LINK)
            .doc(store.state.user.uid),
        userDataJson,
        SetOptions(merge: true),
      );
      newOrdersIds[shopId] = newOrderDocument.id;
    }
    return newOrdersIds;
  }).then((value) {
    print('Transaction then: ${value ?? ""}!');
    store.dispatch(UpdateUserDataAction(userData));
    store.dispatch(ClearCartAction());
    action.response(value, null);
  }).catchError((error) {
    if (error == null) {
      print('Successful transaction!');
    } else {
      print('Transaction error: ${error.toString()}!');
    }
    action.response(null, error);
  });
}

_updatePastOrder(Store<AppState> store, UpdatePastOrderAction action) async {
  final order = action.order;
  final orderJson = order.toJson();
  final orderRef = FirebaseFirestore.instance
      .doc('$SHOPS_LINK/${order.shopId}/orders/${order.id}');
  try {
    await orderRef.update(orderJson);
    NetworkManager.getUserTokens(order.uid).then(
      (tokens) => tokens.forEach(
        (token) => sendNotification(
          token,
          AppLocalizations.of(action.context).translate('order_being_changed'),
          '${AppLocalizations.of(action.context).translate('order')} ${order.orderNumberStringValue()}',
          order.shopId,
          order.id,
        ),
      ),
    );
    store.dispatch(OrderUpdatedAction(order: action.order));
    action.response(null, null);
  } catch (error) {
    action.response(null, error);
  }
}

_signInWithCredential(Store<AppState> store, UserLogInAction action,
    AuthCredential credential) async {
  FirebaseAuth.instance.signInWithCredential(credential).then((authResult) {
    store.dispatch(UserLoggedInAction(authResult.user));
    action.loggedIn();
  }, onError: (e) {
    print(e);
    action.apiErrorCallback();
  }).catchError((error) {
    print(error);
    action.apiErrorCallback();
  });
}

_googleSignIn(Store<AppState> store, GoogleLogInAction action) {
  GoogleSignIn().signIn().then((googleUser) {
    googleUser.authentication.then((googleAuth) {
      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      _signInWithCredential(store, action, credential);
    }).catchError((error) {
      print(error);
      action.apiErrorCallback();
    });
  }).catchError((error) {
    print(error);
    action.apiErrorCallback();
  });
}

_phoneSignIn(Store<AppState> store, PhoneLogInAction action) {
  final AuthCredential credential = PhoneAuthProvider.credential(
      verificationId: action.verificationId, smsCode: action.smsCode);
  _signInWithCredential(store, action, credential);
}
