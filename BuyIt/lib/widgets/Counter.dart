

import 'package:flutter/material.dart';

class Counter extends StatefulWidget {
  final double initValue;
  final double currentValue;
  final Function(double) valueChanged;

  const Counter({
    Key key,
    this.initValue,
    this.currentValue,
    this.valueChanged,
  }) : super(key: key);

  @override
  _CounterState createState() => _CounterState();
}

class _CounterState extends State<Counter> {
  TextEditingController quantityTextEditingController = TextEditingController();

  double currentValue;

  @override
  void initState() {
    super.initState();
    currentValue = widget.currentValue ?? widget.initValue;
    quantityTextEditingController.text = widget.initValue.toString();
  }

  @override
  void dispose() {
    quantityTextEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        IconButton(
            icon: Icon(
              Icons.remove_circle,
              color: Theme.of(context).primaryColor,
            ),
            onPressed: () {
              if (currentValue <= widget.initValue) {
                widget.valueChanged(widget.initValue);
                setState(() {
                  quantityTextEditingController.text = widget.initValue.toStringAsFixed(2);
                });
                return;
              }
              currentValue -= widget.initValue;
              currentValue = double.parse(currentValue.toStringAsFixed(2));
              widget.valueChanged(currentValue);
              setState(() {
                quantityTextEditingController.text = currentValue.toStringAsFixed(2);
              });
            }),
        Expanded(
          child: Container(
            child: Center(
              child: FittedBox(
                child: Text(
                  currentValue.toStringAsFixed(2),
                  style: TextStyle(
                    fontSize: 20.0,
                  ),
                ),
              ),
            ),
          ),
        ),
        IconButton(
            icon: Icon(
              Icons.add_circle,
              color: Theme.of(context).primaryColor,
            ),
            onPressed: () {
              currentValue += widget.initValue;
              currentValue = double.parse(currentValue.toStringAsFixed(2));
              widget.valueChanged(currentValue);
              setState(() {
                quantityTextEditingController.text = currentValue.toStringAsFixed(2);
              });
            }),
      ],
    );
  }
}
