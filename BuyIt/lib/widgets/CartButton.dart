
import 'package:BuyIt/screens/CartScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:BuyIt/model/AppState.dart';
import 'package:BuyIt/model/CartProduct.dart';

class CartButton extends StatelessWidget {
  const CartButton({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, List<CartProduct>>(
        converter: (store) => store.state.cartProducts,
        builder: (context, cartProducts) {
          return Stack(
            children: [
              IconButton(
                icon: Icon(Icons.shopping_cart),
                color: Colors.black38,
                onPressed: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (_) => CartScreen()),
                ),
              ),
              if (cartProducts != null && cartProducts.length > 0)
                Positioned(
                  top: 8.0,
                  right: 8.0,
                  child: Container(
                    width: 6.0,
                    height: 6.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.red,
                    ),
                  ),
                ),
            ],
          );
        });
  }
}
