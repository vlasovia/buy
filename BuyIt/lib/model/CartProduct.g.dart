// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CartProduct.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CartProduct _$CartProductFromJson(Map<String, dynamic> json) {
  return CartProduct(
    json['shopId'] as String,
    json['productId'] as String,
    json['title'] as String,
    json['description'] as String,
    (json['price'] as num).toDouble(),
    (json['quantity'] as num).toDouble(),
  );
}

Map<String, dynamic> _$CartProductToJson(CartProduct instance) =>
    <String, dynamic>{
      'shopId': instance.shopId,
      'productId': instance.productId,
      'title': instance.title,
      'description': instance.description,
      'price': instance.price,
      'quantity': instance.quantity,
    };
