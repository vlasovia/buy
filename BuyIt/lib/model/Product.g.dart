// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Product _$ProductFromJson(Map<String, dynamic> json) {
  return Product(
    id: json['id'] as String,
    title: json['title'] as String,
    description: json['description'] as String,
    price: (json['price'] as num).toDouble(),
    measurement: json['measurement'] as String,
    measurementStep: (json['measurementStep'] as num ?? 1).toDouble(),
    status: ProductStatus.values[json['status'] as int],
    image: json['image'] as String,
  )
    ..productTypeId = json['productTypeId'] as String
    ..imageUrl = json['imageUrl'] as String;
}

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'id': instance.id,
      'productTypeId': instance.productTypeId,
      'title': instance.title,
      'description': instance.description,
      'price': instance.price,
      'measurement': instance.measurement,
      'measurementStep': instance.measurementStep,
      'status': instance.status.index,
      'image': instance.image,
      'imageUrl': instance.imageUrl,
    };
