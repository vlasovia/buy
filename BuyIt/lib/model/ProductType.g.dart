// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ProductType.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductType _$ProductTypeFromJson(Map<String, dynamic> json) {
  return ProductType(
    id: json['id'] as String,
    en: json['en'] as String,
    ru: json['ru'] as String,
    ua: json['ua'] as String,
  );
}

Map<String, dynamic> _$ProductTypeToJson(ProductType instance) =>
    <String, dynamic>{
      'id': instance.id,
      'en': instance.en,
      'ru': instance.ru,
      'ua': instance.ua,
    };
