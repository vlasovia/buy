// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Order.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Order _$OrderFromJson(Map<String, dynamic> json) {
  return Order(
    id: json['id'] as String,
    orderNumber: (json['orderNumber'] as num).toInt(),
    shopId: json['shopId'] as String,
    address: json['address'] as String,
    comment: json['comment'] as String,
    orderStatus: OrderStatus.values[((json['orderStatus'] ?? 0) as int)],
    uid: json['uid'] as String,
    phone: json['phone'] as String,
    deliveryType: (json['deliveryType'] as num).toInt(),
    dateCreated: (json['dateCreated'] as Timestamp).toDate(),
    price: (json['price'] as num).toDouble(),
  );
}

Map<String, dynamic> _$OrderToJson(Order instance) => <String, dynamic>{
      'id': instance.id,
      'orderNumber': instance.orderNumber,
      'shopId': instance.shopId,
      'address': instance.address,
      'comment': instance.comment,
      'orderStatus': instance.orderStatus.index,
      'uid': instance.uid,
      'phone': instance.phone,
      'deliveryType': instance.deliveryType,
      'dateCreated': instance.dateCreated,
      'price': instance.price,
    };
