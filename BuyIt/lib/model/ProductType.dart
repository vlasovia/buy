import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'ProductType.g.dart';

@JsonSerializable(nullable: false)
class ProductType {
  final String id;
  final String en;
  final String ru;
  final String ua;
  ProductType({this.id, this.en, this.ru, this.ua});
  factory ProductType.fromJson(Map<String, dynamic> json) => _$ProductTypeFromJson(json);
  Map<String, dynamic> toJson() => _$ProductTypeToJson(this);
}
