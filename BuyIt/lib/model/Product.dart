import 'package:BuyIt/Common.dart';
import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:flutter/cupertino.dart';
import 'package:json_annotation/json_annotation.dart';

part 'Product.g.dart';

@JsonSerializable(nullable: false)
class Product {
  final String id;
//  'type': Firestore.instance.collection('product_types_test').document(productTypeId),
//  'title': titleTextEditingController.text,
//  'description': descriptionTextEditingController.text,
//  'price': double.parse(priceTextEditingController.text),
//  'measurement': dropdownValue,
//  'image': fileName,
  String productTypeId;
  final String title;
  final String description;
  final double price;
  final ProductStatus status;
  final String measurement;
  final double measurementStep;
  final String image;
  String imageUrl;
  Product({this.id, this.title, this.description, this.price, this.status, this.measurement, this.measurementStep, this.image});
  factory Product.fromJson(Map<String, dynamic> json) => _$ProductFromJson(json);
  Map<String, dynamic> toJson() => _$ProductToJson(this);
}
