

import 'package:json_annotation/json_annotation.dart';
part 'CartProduct.g.dart';

@JsonSerializable(nullable: false)
class CartProduct {

  final String shopId;
  final String productId;
  final String title;
  final String description;
  final double price;
  final double quantity;

  CartProduct(this.shopId, this.productId, this.title, this.description, this.price, this.quantity);

  CartProduct withQuantity(double quantity) {
    return CartProduct(shopId, productId, title, description, price, quantity);
  }

  factory CartProduct.fromJson(Map<String, dynamic> json) => _$CartProductFromJson(json);
  Map<String, dynamic> toJson() => _$CartProductToJson(this);
}