import 'package:firebase_auth/firebase_auth.dart';
import 'package:BuyIt/model/CartProduct.dart';

import 'Order.dart';
import 'ProductType.dart';
import 'Shop.dart';
import 'UserData.dart';

class AppState {
  static var empty = AppState();

  final User user;
  final UserData userData;
  final List<Shop> shops;
  final List<ProductType> productTypes;
  final List<CartProduct> cartProducts;
  final List<Order> orders;

  AppState({
    this.user,
    this.userData,
    this.shops,
    this.productTypes,
    this.cartProducts,
    this.orders,
  });
}
