import 'package:BuyIt/Common.dart';
import 'package:BuyIt/model/CartProduct.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';

part 'Order.g.dart';

@JsonSerializable(nullable: false)
class Order {
  final String id;
  final int orderNumber;
  final String shopId;
  final String address;
  final String comment;
  final DateTime dateCreated;
  final int deliveryType;
  final OrderStatus orderStatus;
  final String uid;
  final String phone;
  final double price;
  List<CartProduct> cartProducts;

  Order({
    this.id,
    this.orderNumber,
    this.shopId,
    this.address,
    this.comment,
    this.dateCreated,
    this.deliveryType,
    this.orderStatus,
    this.uid,
    this.phone,
    this.price,
  });

  factory Order.fromJson(Map<String, dynamic> json) => _$OrderFromJson(json);

  Map<String, dynamic> toJson() => _$OrderToJson(this);

  String orderNumberStringValue() {
    return '№${orderNumber.toString().padLeft(6, '0')}';
  }

  String orderPriceStringValue() {
    return '₴${price.toStringAsFixed(2)}';
  }

  String dateCreatedStringValue() {
    return DateFormat('d.M.y').add_Hm().format(dateCreated);
  }
}
