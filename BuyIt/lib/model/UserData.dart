class UserData {
  final String firstName;
  final String lastName;
  final String address;
  final String myShopId;

  UserData({this.firstName, this.lastName, this.address, this.myShopId});

  factory UserData.fromJson(Map<String, dynamic> json) {
    return UserData(
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      address: json['address'] as String,
      myShopId: json['myShopId'] as String,
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    'firstName': firstName,
    'lastName': lastName,
    'address': address,
  };
}
