// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Shop.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension ShopCopyWithExtension on Shop {
  Shop copyWith({
    String name,
    String owner,
    String titleImage,
    String id,
  }) {
    return Shop(
      name: name ?? this.name,
      description: description ?? this.description,
      phone: phone ?? this.phone,
      address: address ?? this.address,
      owner: owner ?? this.owner,
      titleImage: titleImage ?? this.titleImage,
      id: id ?? this.id,
      available: available ?? this.available,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Shop _$ShopFromJson(Map<String, dynamic> json) {
  return Shop(
    id: json['id'] as String,
    name: json['name'] as String,
    description: json['description'] as String,
    phone: json['phone'] as String,
    address: json['address'] as String,
    owner: json['owner'] as String,
    titleImage: json['titleImage'] as String,
    available: json['available'] as bool,
  )
    ..titleImageUrl = json['titleImageUrl'] as String;
}

Map<String, dynamic> _$ShopToJson(Shop instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'phone': instance.phone,
      'address': instance.address,
      'owner': instance.owner,
      'titleImage': instance.titleImage,
      'titleImageUrl': instance.titleImageUrl,
      'available': instance.available,
    };
