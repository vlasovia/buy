import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:BuyIt/model/Product.dart';

part 'Shop.g.dart';

@CopyWith()
@JsonSerializable(nullable: false)
class Shop {
  final String id;
  final String name;
  final String description;
  final String phone;
  final String address;
  final String owner;
  final String titleImage;
  final bool available;
  String titleImageUrl;
  List<Product> products;

  Shop({this.id, this.name, this.description, this.phone, this.address, this.owner, this.titleImage, this.available});
  factory Shop.fromJson(Map<String, dynamic> json) => _$ShopFromJson(json);
  Map<String, dynamic> toJson() => _$ShopToJson(this);
}
