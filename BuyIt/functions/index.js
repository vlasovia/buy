const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

//import admin module
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

// exports.pushNotification = functions.database.ref('/YourNode/{pushId}').onWrite((change, context) => {
//     console.log('Push notification event triggered');

//     //  Get the current value of what was written to the Realtime Database.
//     const valueObject = change.after.val(); 

//     // Create a notification
//     const payload = {
//         notification: {
//             title: valueObject.title,
//             body: valueObject.message,
//             sound: "default"
//         }
//     };

//     //Create an options object that contains the time to live for the notification and the priority
//     const options = {
//         priority: "high",
//         timeToLive: 60 * 60 * 24
//     };

//     return admin.messaging().sendToTopic("pushNotifications", payload, options);
// });


exports.createUser = functions.firestore
    .document('shopsSandbox/{shopId}/orders/{orderId}')
    .onCreate((snap, context) => {
		// Get an object representing the document
		// e.g. {'name': 'Marie', 'age': 66}
		const orderData = snap.data();

		// access a particular field as you would any JS property
		console.log('Push notification event triggered');
		console.log(orderData.uid);

		const userDataPromise = admin.firestore().collection('usersSandbox').doc(orderData.uid).get();
		return userDataPromise.then((userSnap) => {
			console.log(`Data: ${userSnap.data()}`);
			console.log('User ' + orderData.uid + ' token: ' + userSnap.data().token);
			// Create a notification
			const payload = {
			notification: {
			    title: 'Title',
			    body: 'Message',
			    sound: "default"
				}
			};

			//Create an options object that contains the time to live for the notification and the priority
			const options = {
				priority: "high",
				timeToLive: 60 * 60 * 24
			};

			return admin.messaging().sendToDevice(userSnap.data().token, payload, options);
		}).catch(err => {
		    console.log('Error getting document', err);
		});
      // perform desired operations ...
    });
